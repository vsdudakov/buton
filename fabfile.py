#-*- encoding: utf-8 -*-
import fabric
import os

HOME_PATH = os.path.join('/home', 'r', 'ramnaru', 'salonbuton', 'public_html')

fabric.api.env.roledefs['production'] = ['77.222.57.205']
#https://sweb.ru/


def production_env():
    fabric.api.env.user = 'ramnaru'
    fabric.api.env.password = '924042sa'
    fabric.api.env.project_root = os.path.join(HOME_PATH, 'buton')
    fabric.api.env.shell = '/bin/bash -c'
    fabric.api.env.python = os.path.join(HOME_PATH, 'buton_v', 'bin', 'python')
    fabric.api.env.pip = os.path.join(HOME_PATH, 'buton_v', 'bin', 'pip')
    fabric.api.env.uwsgi = os.path.join(HOME_PATH, 'buton_v', 'bin', 'uwsgi')  
    fabric.api.env.always_use_pty = False


def deploy():
    with fabric.api.cd(fabric.api.env.project_root):
        fabric.api.run('git pull origin master')
        fabric.api.run('{pip} install -r requirements.txt'.format(pip=fabric.api.env.pip))
        fabric.api.run('{python} manage.py initenv'.format(python=fabric.api.env.python))


@fabric.api.roles('production')
def prod():
    production_env()
    deploy()