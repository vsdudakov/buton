#-*- encoding: utf-8 -*-
from ckeditor_uploader.fields import RichTextUploadingField

from django.db import models
from django.utils.translation import ugettext_lazy as _ul
from django.contrib.auth.models import (
    AbstractUser,
    UserManager,
)
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.core.validators import validate_email

from main.models import DateBaseModel


class ExtUserManager(UserManager):

    def send_feedback(self, site, feedback):
        site_profile = site.get_profile()
        host = site_profile.http_methog + site.domain
        support_email = site_profile.support_email
        send_mail(
            subject=_ul(u'Вам написал пользователь %s %s на %s') % (feedback.name, feedback.phone, host),
            message=feedback.question,
            from_email=support_email,
            recipient_list=[support_email],
            fail_silently=True,
            html_message=feedback.question
            )

    def send_call(self, site, call):
        site_profile = site.get_profile()
        host = site_profile.http_methog + site.domain
        support_email = site_profile.support_email
        subject = _ul(u'Вам заказал звонок пользователь %s %s на %s') % (call.name, call.phone, host)
        send_mail(
            subject=subject,
            message=subject,
            from_email=support_email,
            recipient_list=[support_email],
            fail_silently=True,
            html_message=subject
            )

    def send_resetpassword(self, site, user):
        site_profile = site.get_profile()
        host = site_profile.http_methog + site.domain
        support_email = site_profile.support_email
        reset_url = host + reverse('reset-password-complete', kwargs={'uuid': user.reset_password_key})
        message = _ul(u"""
            Для сброса пароль перейдите по следующей ссылке:\n
            %s \n
            \n
            С уважением,
            Ваш %s
            """) % (reset_url, host)
        send_mail(
            subject=_ul(u'Сброс пароля для пользователя %s на %s') % (user.get_full_name(), host),
            message=message,
            from_email=support_email,
            recipient_list=[user.email],
            fail_silently=True
            )

    def send_registration(self, site, user):
        site_profile = site.get_profile()
        host = site_profile.http_methog + site.domain
        support_email = site_profile.support_email
        reset_url = host + reverse('registration-complete', kwargs={'uuid': user.registration_key})
        message = _ul(u"""
            Для завершения регистрации перейдите по следующей ссылке:\n
            %s \n
            \n
            С уважением,
            Ваш %s
            """) % (reset_url, host)
        send_mail(
            subject=_ul(u'Завершения регистрации для пользователя %s на %s') % (user.get_full_name(), host),
            message=message,
            from_email=support_email,
            recipient_list=[user.email],
            fail_silently=True
            )

    def create_order(self, site, order):
        site_profile = site.get_profile()
        host = site_profile.http_methog + site.domain
        support_email = site_profile.support_email
        order_url = host + reverse('order-detail', kwargs={'pk': order.pk})
        message = _ul(u"""
            Вами был оформлен заказ №%s на сайте %s \n
            Дата и время заказа: %s\n
            Для просмотра и/или оплаты заказа перейдите по следующей ссылке:\n
            %s \n
            \n
            С уважением,
            Ваш %s
            """) % (
            order.pk, 
            host, 
            order.creation_date.strftime('%d.%m.%Y %H:%M'),
            order_url, 
            host
            )
        send_mail(
            subject=_ul(u'Вами был оформлен заказ №%s на сайте %s') % (order.pk, host),
            message=message,
            from_email=support_email,
            recipient_list=[order.email],
            fail_silently=True
            )


        manager_email = site_profile.manager_email
        if manager_email:
            items = ''
            for item in order.order_items.values('item__title', 'count'):
                items += '    %(item__title)s - %(count)s\n' % item
            message = _ul(u"""
                Заказ №%s был оформлен на сайте %s \n
                Информация заказа: \n
                №: %s,\n
                Товары: \n%s\n
                Цена: %s,\n
                Скидка: %s,\n
                Цена со скидкой: %s,\n
                Способ доставки: %s,\n
                Адрес доставки: %s,\n
                Способ оплаты: %s,\n
                Дата и время заказа: %s,\n
                \n
                Перейти на детали заказа: %s\n
                С уважением,
                Ваш %s
                """) % (
                    order.pk, 
                    host, 
                    order.pk, 
                    items,
                    order.items_price,
                    order.items_discount,
                    order.items_price_with_discount,
                    order.get_delivery_type_display(),
                    order.delivery,
                    order.get_payment_type_display(),
                    order.creation_date.strftime('%d.%m.%Y %H:%M'),
                    order_url, 
                    host
                    )
            send_mail(
                subject=_ul(u'Создан новый заказ №%s на сайте %s') % (order.pk, host),
                message=message,
                from_email=support_email,
                recipient_list=[manager_email],
                fail_silently=True
                )




class User(AbstractUser):
    delivery = models.TextField(verbose_name=_ul(u'Адрес доставки'), null=True, blank=True)
    phone = models.CharField(verbose_name=_ul(u'Номер телефона (+79101010101)'), max_length=255, null=True, blank=True)
    birthday = models.DateField(verbose_name=_ul(u'День рождения'), null=True, blank=True)

    reset_password_key = models.CharField(max_length=255, verbose_name=_ul(u'Ключ сброса пароля'), null=True, blank=True)
    registration_key = models.CharField(max_length=255, verbose_name=_ul(u'Ключ регистрации'), null=True, blank=True)

    objects = ExtUserManager()

    @property
    def members(self):
        return FamilyMember.objects.filter(user=self)

    def get_full_name(self):
        full_name = super(User, self).get_full_name()
        if not full_name:
            return self.username 
        return full_name

    def save(self, *args, **kwargs):
        try:
            validate_email(self.username)
            self.email = self.username
        except Exception:
            pass
        return super(User, self).save(*args, **kwargs)


class FamilyMember(DateBaseModel):
    user = models.ForeignKey(User, verbose_name=_ul(u'Пользователь'))
    name = models.CharField(verbose_name=_ul(u'Имя'), max_length=255)
    m_birthday = models.DateField(verbose_name=_ul(u'День рождения'))

    @property
    def birthday(self):
        from django.utils.dateformat import DateFormat
        df = DateFormat(self.m_birthday)
        return df.format('d.m.y')
    

    class Meta:
        verbose_name = _ul(u'Член семью')
        verbose_name_plural = _ul(u'Члены семьи')

    def __unicode__(self):
        return u'%s' % self.name