#-*- encoding: utf-8 -*-
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _ul


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = _ul(u"Пользователи")
