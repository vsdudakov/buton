#-*- encoding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.forms import UserCreationForm as BaseUserCreationForm
from django.contrib.auth.forms import UserChangeForm as BaseUserChangeForm 
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth import forms
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _ul

from users.models import User, FamilyMember


class UserCreationForm(BaseUserCreationForm):
    
    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    class Meta(BaseUserChangeForm.Meta):
        model = User


class FamilyMemberInline(admin.TabularInline):
    model = FamilyMember
    

class UserAdmin(BaseUserAdmin):
    add_form = UserCreationForm 

    fieldsets = BaseUserAdmin.fieldsets + (
        (_ul(u'Личная информация'), {
            'fields': (
                'delivery',
                'phone',
                'birthday',
                )
        }),
        (_ul(u'Токены'), {
            'fields': (
                'reset_password_key',
                'registration_key',
                )
        }),
    )

    inlines = (
        FamilyMemberInline,
    )


admin.site.register(User, UserAdmin)
admin.site.unregister(Group)
