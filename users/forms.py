#-*- encoding: utf-8 -*-
from django import forms
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _ul

from main.forms import BootstrapForm, BootstrapModelForm
from users.models import User, FamilyMember


class LoginForm(BootstrapForm):
    username = forms.EmailField(max_length=255, label=_ul(u'Электронная почта'))
    password = forms.CharField(max_length=255, widget=forms.PasswordInput(), label=_ul(u'Пароль'))

    def clean(self):
        self.cleaned_data = super(LoginForm, self).clean()
        if 'password' in self.cleaned_data and 'username' in self.cleaned_data:
            self.cleaned_data['user'] = authenticate(username=self.cleaned_data['username'], password=self.cleaned_data['password'])
            if not self.cleaned_data['user']:
                raise forms.ValidationError(_ul(u'Не правильно введен логин или пароль.'))
            if not self.cleaned_data['user'].is_active:
                raise forms.ValidationError(_ul(u'Сначала завершите регистрацию.'))
        return self.cleaned_data


class RegistrationForm(BootstrapModelForm):
    username = forms.EmailField(max_length=255, label=_ul(u'Электронная почта'))
    password = forms.CharField(max_length=255, widget=forms.PasswordInput(), label=_ul(u'Пароль'))
    confirm_password = forms.CharField(max_length=255, widget=forms.PasswordInput(), label=_ul(u'Пароль еще раз'))

    def pre_init(self, *args, **kwargs):
        self.fields['first_name'].required = True

    def clean_confirm_password(self):
        if self.cleaned_data.get('password', None) is None:
            return
        if self.cleaned_data.get('confirm_password', None) is None:
            return
        if not self.cleaned_data['password'] == self.cleaned_data['confirm_password']:
            raise forms.ValidationError(_ul(u'Пароли не совпадают.'))
        return self.cleaned_data['confirm_password']

    class Meta:
        model = User
        fields = (
            'username', 
            'password',
            'confirm_password',
            'first_name', 
            'last_name', 
            'phone'
            )


class ResetPasswordForm(BootstrapForm):
    email = forms.EmailField(max_length=255, label=_ul(u'Электронная почта'))

    def clean_email(self):
        if self.cleaned_data.get('email', None) is None:
            return
        self.cleaned_data['user'] = User.objects.filter(username=self.cleaned_data['email']).first()
        if self.cleaned_data['user'] is None:
            raise forms.ValidationError(_ul(u'Пользователь с такой электронной почтой не существует.'))
        return self.cleaned_data['email'] 


class ResetPasswordCompleteForm(BootstrapForm):
    password = forms.CharField(max_length=255, widget=forms.PasswordInput(), label=_ul(u'Новый пароль'))
    confirm_password = forms.CharField(max_length=255, widget=forms.PasswordInput(), label=_ul(u'Новый пароль еще раз'))
 
    def clean_confirm_password(self):
        if self.cleaned_data.get('password', None) is None:
            return
        if self.cleaned_data.get('confirm_password', None) is None:
            return
        if not self.cleaned_data['password'] == self.cleaned_data['confirm_password']:
            raise forms.ValidationError(_ul(u'Пароли не совпадают.'))
        return self.cleaned_data['confirm_password']


class ChangePasswordForm(ResetPasswordCompleteForm):
    pass


class ProfileForm(BootstrapModelForm):

    def pre_init(self, *args, **kwargs):
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['phone'].required = True
        self.fields['email'].widget = forms.HiddenInput()
        self.fields['username'].widget = forms.HiddenInput()

    class Meta:
        model = User
        fields = (
            'username',
            'email', 
            'first_name', 
            'last_name', 
            'phone',
            'birthday',
            'delivery',
            )


class ProfileFamilyForm(BootstrapModelForm):

    def __init__(self, user, *args, **kwargs):
        super(ProfileFamilyForm, self).__init__(*args, **kwargs)
        self.user = user

    def clean(self):
        cleaned_data = super(ProfileFamilyForm, self).clean()
        if FamilyMember.objects.filter(user=self.user).count() >=5:
            raise forms.ValidationError(_ul(u'Вы можете добавлять не более 5 членов семьи.'))
        return cleaned_data

    def save(self, *args, **kwargs):
        self.instance.user = self.user
        return super(ProfileFamilyForm, self).save(*args, **kwargs)

    class Meta:
        model = FamilyMember
        fields = ('name', 'm_birthday')
