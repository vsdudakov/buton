#-*- encoding: utf-8 -*-
import uuid

from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import FormView, RedirectView, TemplateView, CreateView, UpdateView, DetailView
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import logout, login, authenticate
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site

from users.forms import (
    LoginForm, 
    RegistrationForm, 
    ResetPasswordForm, 
    ResetPasswordCompleteForm,
    ChangePasswordForm,
    ProfileForm,
    ProfileFamilyForm
    )
from users.models import User, FamilyMember
from main.decorators import login_required
from main.mixins import DialogFormMixin, get_and_reset_session


class LoginView(DialogFormMixin, FormView):
    form_class = LoginForm
    success_url = reverse_lazy('index')

    def get_success_url(self):
        success_url = self.request.GET.get('next')
        if success_url:
            return success_url
        return self.success_url

    def form_valid(self, form):
        user = form.cleaned_data['user']
        if user is not None:
            login(self.request, user)
        return super(LoginView, self).form_valid(form)


class LogoutView(RedirectView):
    url = reverse_lazy('index')

    def get_redirect_url(self):
        logout(self.request)
        redirect_url = self.request.GET.get('next')
        if redirect_url:
            return redirect_url
        return self.url

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(LogoutView, self).dispatch(*args, **kwargs)


class RegistrationView(DialogFormMixin, FormView):
    form_class = RegistrationForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        key = uuid.uuid4()
        user = form.save()
        user.set_password(form.cleaned_data['password'])
        user.is_active = False
        user.email = user.username
        user.registration_key = key
        user.save()
        messages.success(self.request, u'На вашу почту было отправлено письмо со ссылкой. Перейдите по ссылке для завершения регистрации.')
        User.objects.send_registration(get_current_site(self.request), user)
        return super(RegistrationView, self).form_valid(form)


class RegistrationCompleteView(RedirectView):

    url = reverse_lazy('profile')

    def get_redirect_url(self, uuid):
        user = get_object_or_404(User, registration_key=uuid)
        user.is_active = True
        user.registration_key = None
        user.save()
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, user)
        return super(RegistrationCompleteView, self).get_redirect_url()


class ResetPasswordView(FormView):
    template_name = 'main/reset-password.html'
    form_class = ResetPasswordForm
    success_url = reverse_lazy('reset-password')

    def form_valid(self, form):
        key = uuid.uuid4()
        user = form.cleaned_data['user']
        user.reset_password_key = key
        user.save()
        messages.success(self.request, u'На вашу почту было отправлено письмо со ссылкой. Перейдите по ссылке для сброса пароля.')
        User.objects.send_resetpassword(get_current_site(self.request), user)
        return super(ResetPasswordView, self).form_valid(form)


class ResetPasswordCompleteView(FormView):
    template_name = 'main/reset-password-complete.html'
    form_class = ResetPasswordCompleteForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        user = get_object_or_404(User, reset_password_key=self.kwargs['uuid'])
        user.set_password(form.cleaned_data['password'])
        user.reset_password_key = None
        user.save()
        return super(ResetPasswordCompleteView, self).form_valid(form)


class ProfileView(DetailView):
    template_name = 'main/profile.html'
    model = User

    def get_object(self, queryset=None):
        return self.request.user

    def get_context_data(self, **kwargs):
        kwargs = super(ProfileView, self).get_context_data(**kwargs)
        kwargs['profile_form'] = ProfileForm(instance=self.request.user, data=get_and_reset_session(self.request, 'ProfileForm'))
        kwargs['password_form'] = ChangePasswordForm(data=get_and_reset_session(self.request, 'ChangePasswordForm'))
        kwargs['member_form'] = ProfileFamilyForm(user=self.request.user, data=get_and_reset_session(self.request, 'ProfileFamilyForm'))
        return kwargs

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(ProfileView, self).dispatch(*args, **kwargs)


class ChangePasswordView(DialogFormMixin, FormView):
    form_class = ChangePasswordForm
    success_url = reverse_lazy('index')
    unsuccess_url = reverse_lazy('profile')

    def form_valid(self, form):
        self.request.user.set_password(form.cleaned_data['password'])
        self.request.user.save()
        messages.success(self.request, u'Пароль успешно изменен. Войдите с новым паролем.')
        return super(ChangePasswordView, self).form_valid(form)

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(ChangePasswordView, self).dispatch(*args, **kwargs)


class ProfileUpdateView(DialogFormMixin, UpdateView):
    model = User
    form_class = ProfileForm
    success_url = reverse_lazy('profile')

    def get_object(self, queryset=None):
        return self.request.user

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(ProfileUpdateView, self).dispatch(*args, **kwargs)


class ProfileMemberCreateView(DialogFormMixin, CreateView):
    model = FamilyMember
    form_class = ProfileFamilyForm
    success_url = reverse_lazy('profile')

    def get_form_kwargs(self, **kwargs):
        kwargs = super(ProfileMemberCreateView, self).get_form_kwargs(**kwargs)
        kwargs['user'] = self.request.user
        return kwargs

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(ProfileMemberCreateView, self).dispatch(*args, **kwargs)


class ProfileMemberUpdateView(DialogFormMixin, UpdateView):
    model = FamilyMember
    form_class = ProfileFamilyForm
    success_url = reverse_lazy('profile')

    def get_form_kwargs(self, **kwargs):
        kwargs = super(ProfileMemberUpdateView, self).get_form_kwargs(**kwargs)
        kwargs['user'] = self.request.user
        return kwargs

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(ProfileMemberUpdateView, self).dispatch(*args, **kwargs)


