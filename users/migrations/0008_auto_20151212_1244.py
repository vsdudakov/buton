# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_auto_20151203_0945'),
    ]

    operations = [
        migrations.RenameField(
            model_name='familymember',
            old_name='birthday',
            new_name='m_birthday',
        ),
    ]
