# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20151018_2301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='delivery',
            field=models.TextField(null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438', blank=True),
        ),
    ]
