# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20151018_0044'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='registration_key',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041a\u043b\u044e\u0447 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='reset_password_key',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041a\u043b\u044e\u0447 \u0441\u0431\u0440\u043e\u0441\u0430 \u043f\u0430\u0440\u043e\u043b\u044f', blank=True),
        ),
    ]
