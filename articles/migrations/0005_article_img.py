# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import articles.models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0004_auto_20151103_0958'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='img',
            field=models.ImageField(upload_to=articles.models.img_upload_to, null=True, verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430', blank=True),
        ),
    ]
