#-*- encoding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _ul

from articles.models import Category, Article


class CategoryAdmin(admin.ModelAdmin):

    fieldsets = (
        (None, {
            'fields': (
                'title',
                'description',
                'is_published',
            )
        }),
        (_ul(u'Seo'), {
            'fields': (
                'slug',
                'seo_title',
                'seo_description', 
                'seo_keywords', 
                'seo_author'
                )
        }),
    )


class ArticleAdmin(admin.ModelAdmin):

    fieldsets = (
        (None, {
            'fields': (
                'category',
                'img',
                'alt',
                'title',
                'description',
                'announcement',
                'is_published',
            )
        }),
        (_ul(u'Seo'), {
            'fields': (
                'slug',
                'seo_title',
                'seo_description', 
                'seo_keywords', 
                'seo_author'
                )
        }),
    )


admin.site.register(Category, CategoryAdmin)
admin.site.register(Article, ArticleAdmin)