#-*- encoding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView

from main.mixins import SeoMixin
from articles.models import Category, Article


class ArticleList(SeoMixin, ListView):
    template_name = 'main/articles.html'
    paginate_by = 10
    model = Article

    def get_queryset(self):
        qs = super(ArticleList, self).get_queryset()
        qs = qs.filter(is_published=True, category__is_published=True)
        qs = qs.filter(category=self.category).order_by('-creation_date')
        return qs

    def get_context_data(self, **kwargs):
        kwargs = super(ArticleList, self).get_context_data(**kwargs)
        kwargs['object'] = self.category
        kwargs = self.get_context_seo_data(**kwargs) 
        return kwargs

    def dispatch(self, request, *args, **kwargs):
        slug = kwargs.get('slug', None)
        if slug is None:
            self.category = Category.objects.filter(is_published=True).first()
        else:
            self.category = get_object_or_404(Category, slug=slug, is_published=True)
        return super(ArticleList, self).dispatch(request, *args, **kwargs)


class ArticleDetail(SeoMixin, DetailView):
    template_name = 'main/article.html'
    model = Article

    def get_object(self, queryset=None):
        qs = self.model.objects.all()
        qs = qs.filter(is_published=True)
        return super(ArticleDetail, self).get_object(queryset=qs)

    def get_context_data(self, **kwargs):
        kwargs = super(ArticleDetail, self).get_context_data(**kwargs)
        kwargs = self.get_context_seo_data(**kwargs) 
        return kwargs