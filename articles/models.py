#-*- encoding: utf-8 -*-
import os
from ckeditor_uploader.fields import RichTextUploadingField

from django.db import models
from django.utils.translation import ugettext_lazy as _ul

from main.models import SeoBaseModel


class Category(SeoBaseModel):
    description = RichTextUploadingField(verbose_name=_ul(u'Описание'), null=True, blank=True)

    class Meta:
        verbose_name = _ul(u'Категория')
        verbose_name_plural = _ul(u'Категории')

    def __unicode__(self):
        return u'%s' % self.title


def img_upload_to(instance, filename):
    UPLOAD_PATH = 'uploads/articles/'
    return os.path.join(UPLOAD_PATH, instance.slug, filename)


class Article(SeoBaseModel):
    img = models.ImageField(verbose_name=_ul(u'Картинка'), upload_to=img_upload_to, null=True, blank=True)
    alt = models.CharField(verbose_name=_ul(u'Alt картинки'), max_length=255, null=True, blank=True)
    category = models.ForeignKey(Category, verbose_name=_ul(u'Категория'))
    announcement = RichTextUploadingField(verbose_name=_ul(u'Анонс'), null=True, blank=True)
    description = RichTextUploadingField(verbose_name=_ul(u'Описание'), null=True, blank=True)

    class Meta:
        verbose_name = _ul(u'Статья')
        verbose_name_plural = _ul(u'Статьи')

    def __unicode__(self):
        return u'%s' % self.title