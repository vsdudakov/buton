#-*- encoding: utf-8 -*-
import os
from ckeditor_uploader.fields import RichTextUploadingField

from django.db import models
from django.utils.translation import ugettext_lazy as _ul
from django.forms.models import model_to_dict
from django.core.exceptions import ValidationError

from main.models import SeoBaseModel, DateBaseModel
from users.models import User


class Catalog(SeoBaseModel):
    description = RichTextUploadingField(verbose_name=_ul(u'Описание'), null=True, blank=True)
    sorting = models.PositiveIntegerField(verbose_name=_ul(u'Сортировка'), default=0)

    class Meta:
        verbose_name = _ul(u'Каталог')
        verbose_name_plural = _ul(u'Каталоги')

    def __unicode__(self):
        return u'%s' % self.title


class ItemManager(models.Manager):
    pass


class Item(SeoBaseModel):
    catalog = models.ForeignKey(Catalog, verbose_name=_ul(u'Каталог'))
    description = RichTextUploadingField(verbose_name=_ul(u'Описание'), null=True, blank=True)
    price = models.DecimalField(verbose_name=_ul(u'Цена'), default=0.0, max_digits=10, decimal_places=0)
    sorting = models.PositiveIntegerField(verbose_name=_ul(u'Сортировка'), default=0)

    objects = ItemManager()

    @property
    def image(self):
        return ItemImage.objects.filter(item=self, is_main=True).first()

    @property
    def images(self):
        return ItemImage.objects.filter(item=self)

    @property
    def parts(self):
        return ItemPart.objects.filter(item=self)

    class Meta:
        verbose_name = _ul(u'Товар')
        verbose_name_plural = _ul(u'Товары')

    def __unicode__(self):
        return u'%s' % self.title


def img_upload_to(instance, filename):
    UPLOAD_PATH = 'uploads/items/'
    return os.path.join(UPLOAD_PATH, instance.item.slug, filename)


class ItemImage(DateBaseModel):
    item = models.ForeignKey(Item, verbose_name=_ul(u'Товар'))
    is_main = models.BooleanField(verbose_name=_ul(u'Главная?'), default=False)
    img = models.ImageField(verbose_name=_ul(u'Картинка'), upload_to=img_upload_to)
    alt = models.CharField(verbose_name=_ul(u'Alt картинки'), max_length=255, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.is_main:
            self.__class__.objects.filter(item=self.item).update(is_main=False)
        return super(ItemImage, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _ul(u'Картинка товара')
        verbose_name_plural = _ul(u'Картинки товара')

    def __unicode__(self):
        return u'%s: %s' % (self.item.title, self.img)


class ItemPart(DateBaseModel):
    item = models.ForeignKey(Item, verbose_name=_ul(u'Товар'))
    name = models.CharField(verbose_name=_ul(u'Наименование'), max_length=255)
    count = models.PositiveIntegerField(verbose_name=_ul(u'Кол-во'), default=0)

    class Meta:
        verbose_name = _ul(u'Состав товара')
        verbose_name_plural = _ul(u'Состав товара')

    def __unicode__(self):
        return u'%s: %s' % (self.item.title, self.name)


class Discount(DateBaseModel):
    DISCOUNT_ACTION_AUTH = 0
    DISCOUNT_ACTION_FAMILY_BIRTHDAY = 1
    DISCOUNT_ACTION_PRE_ORDER = 2
    DISCOUNT_ACTIONS = (
        (DISCOUNT_ACTION_AUTH, _ul(u'Скидка для авторизованного пользователя')),
        (DISCOUNT_ACTION_FAMILY_BIRTHDAY, _ul(u'Скидка в день рождения члена семьи')),
        (DISCOUNT_ACTION_PRE_ORDER, _ul(u'Скидка на предварительный заказ')),
    )

    # DISCOUNT_PERCENTAGE = 0
    # DISCOUNT_AMOUNT = 1
    # DISCOUNT_TYPES = (
    #     (DISCOUNT_PERCENTAGE, _ul(u'Скидка в процентах')),
    #     (DISCOUNT_AMOUNT, _ul(u'Скидка в рублях')),
    # )

    action = models.PositiveIntegerField(verbose_name=_ul(u'Вариант скидки'), choices=DISCOUNT_ACTIONS, unique=True)
    #discount_type = models.PositiveIntegerField(verbose_name=_ul(u'Тип скидки'), default=DISCOUNT_PERCENTAGE, choices=DISCOUNT_TYPES)
    amount = models.DecimalField(verbose_name=_ul(u'Скидка в %'), default=0.0, max_digits=10, decimal_places=0)
    active = models.BooleanField(verbose_name=_ul(u'Активирована?'), default=False)

    class Meta:
        verbose_name = _ul(u'Скидка')
        verbose_name_plural = _ul(u'Скидки')

    def __unicode__(self):
        return u'%s - %s' % (self.get_action_display(), self.amount)


class Order(DateBaseModel):
    ORDER_STATUS_NEW = 0
    ORDER_STATUS_PAYMENT = 1
    ORDER_STATUS_COMPLETED = 2
    ORDER_STATUSES = (
        (ORDER_STATUS_NEW, _ul(u'Новый')),
        (ORDER_STATUS_PAYMENT, _ul(u'Оплаченный')),
        (ORDER_STATUS_COMPLETED, _ul(u'Завершенный')),
    )

    DELIVERY_SELF = 0
    DELIVERY_DOOR = 1
    DELIVERY_TYPES = (
        (DELIVERY_SELF, _ul(u'Самовывоз')),
        (DELIVERY_DOOR, _ul(u'Доставка до двери')),
    )

    PAYMENT_CASH = 0
    PAYMENT_CREDIT_CARD = 1
    PAYMENT_ECOMMERCE = 2
    PAYMENT_TYPES = (
        (PAYMENT_CASH, _ul(u'Наличные')),
        (PAYMENT_CREDIT_CARD, _ul(u'Банковская карта')),
        (PAYMENT_ECOMMERCE, _ul(u'Электронные деньги')),
    )

    user = models.ForeignKey(User, verbose_name=_ul(u'Пользователь'), null=True, blank=True)
    first_name = models.CharField(verbose_name=_ul(u'Имя'), max_length=255)
    last_name = models.CharField(verbose_name=_ul(u'Фамилия'), max_length=255, null=True, blank=True)
    phone = models.CharField(verbose_name=_ul(u'Номер телефона (+79101010101)'), max_length=255, null=True, blank=True)
    email = models.EmailField(verbose_name=_ul(u'Адрес электронной почты'), max_length=255, null=True, blank=True)
    delivery_type = models.PositiveIntegerField(verbose_name=_ul(u'Вариант доставки'), default=DELIVERY_SELF, choices=DELIVERY_TYPES)
    delivery = models.TextField(verbose_name=_ul(u'Адрес доставки'), null=True, blank=True)
    delivery_date = models.DateTimeField(verbose_name=_ul(u'Дата доставки'), null=True, blank=True)
    payment_type = models.PositiveIntegerField(verbose_name=_ul(u'Вариант оплаты'), default=PAYMENT_CASH, choices=PAYMENT_TYPES)
    note = models.TextField(verbose_name=_ul(u'Комментарии'), null=True, blank=True)

    items_count = models.PositiveIntegerField(verbose_name=_ul(u'Кол-во'), null=True, blank=True)
    items_price = models.DecimalField(verbose_name=_ul(u'Цена'), max_digits=10, decimal_places=0, null=True, blank=True)
    items_discount = models.DecimalField(verbose_name=_ul(u'Скидка в %'), max_digits=10, decimal_places=0, null=True, blank=True)
    items_price_with_discount = models.DecimalField(verbose_name=_ul(u'Цена cо скидкой'), max_digits=10, decimal_places=0, null=True, blank=True)

    status = models.PositiveIntegerField(verbose_name=_ul(u'Статус'), default=ORDER_STATUS_NEW, choices=ORDER_STATUSES)

    items = models.ManyToManyField(Item, verbose_name=_ul(u'Товары'), through='OrderItem', blank=True)

    @property
    def order_items(self):
        return OrderItem.objects.filter(order=self)

    def clean(self):
        if not self.email and not self.phone:
            raise ValidationError(_ul(u'Телефон или почта должны быть заполнены.'))

    class Meta:
        verbose_name = _ul(u'Заказ')
        verbose_name_plural = _ul(u'Заказы')

    def __unicode__(self):
        return u'%s: %s %s - %s' % (self.creation_date, self.first_name, self.last_name, self.get_status_display())


class OrderItemManager(models.Manager):

    def get_cart(self, request, is_dict=False):
        #request.session['cart'] = {}
        order = Order()
        cart = request.session.get('cart', {})
        items = Item.objects.filter(pk__in=cart.keys())
        order_items = []
        for item in items:
            order_item = OrderItem(
                order=order,
                item=item,
                item_catalog=item.catalog,
                item_title=item.title,
                item_description=item.description,
                item_price=item.price,
                count=cart[str(item.pk)]
                )
            if is_dict:
                order_item = model_to_dict(order_item)
                order_item['item'] = model_to_dict(item)
                order_item['order'] = model_to_dict(order)
            order_items.append(
                order_item
                )
        return order_items


class OrderItem(DateBaseModel):
    order = models.ForeignKey(Order, verbose_name=_ul(u'Заказ'))
    item = models.ForeignKey(Item, verbose_name=_ul(u'Товар'))

    item_catalog = models.ForeignKey(Catalog, verbose_name=_ul(u'Каталог'))
    item_title = models.CharField(verbose_name=_ul(u'Заголовок'), max_length=255)
    item_description = RichTextUploadingField(verbose_name=_ul(u'Описание'), null=True, blank=True)
    item_price = models.DecimalField(verbose_name=_ul(u'Цена'), default=0.0, max_digits=10, decimal_places=0)

    count = models.PositiveIntegerField(verbose_name=_ul(u'Кол-во'), default=1)

    objects = OrderItemManager()

    class Meta:
        verbose_name = _ul(u'Товар заказа')
        verbose_name_plural = _ul(u'Товары заказа')
        unique_together = ('order', 'item')

    def __unicode__(self):
        return u'%s / %s' % (self.order, self.item)


