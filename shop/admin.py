#-*- encoding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _ul
from django.utils.safestring import mark_safe

from shop.models import Catalog, Item, ItemImage, ItemPart, Order, OrderItem, Discount


class CatalogAdmin(admin.ModelAdmin):

    fieldsets = (
        (None, {
            'fields': (
                'title',
                'description',
                'is_published',
            )
        }),
        (_ul(u'Seo'), {
            'fields': (
                'slug',
                'seo_title',
                'seo_description',
                'seo_keywords',
                'seo_author'
                )
        }),
    )


class ItemImageInline(admin.TabularInline):
    model = ItemImage


class ItemPartInline(admin.TabularInline):
    model = ItemPart


def main_image(obj):
    image = obj.image
    if image:
        return mark_safe(u'<img src="%s" width="50" height="50" />' % image.img.url)
main_image.short_description = _ul(u'главная картинка')
main_image.allow_tags = True


class ItemAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        main_image,
    )

    fieldsets = (
        (None, {
            'fields': (
                'catalog',
                'title',
                'description',
                'price',
                'is_published',
            )
        }),
        (_ul(u'Seo'), {
            'fields': (
                'slug',
                'seo_title',
                'seo_description',
                'seo_keywords',
                'seo_author'
                )
        }),
    )

    inlines = (
        ItemImageInline,
        ItemPartInline,
    )


class OrderItemInline(admin.StackedInline):
    model = OrderItem


class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'creation_date',
        'first_name',
        'last_name',
        'phone',
        'email',
        'delivery_type',
        'payment_type'
    )

    inlines = (
        OrderItemInline,
    )



admin.site.register(Catalog, CatalogAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Discount)
admin.site.register(Order, OrderAdmin)