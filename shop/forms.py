#-*- encoding: utf-8 -*-
import datetime

from django import forms
from django.utils.translation import ugettext_lazy as _ul
from django.utils import timezone

from main.forms import BootstrapForm, BootstrapModelForm
from shop.models import Order


class OrderForm(BootstrapModelForm):

    #delivery_date_widget = forms.CharField(required = True)

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        #self.fields['delivery_date_widget'].initial = get_delivery_date()
        self.fields['delivery_date'].required = True
        self.fields['delivery_date'].widget.attrs['class'] = self.fields['delivery_date'].widget.attrs.get('class', '') + u' cart-discounts'
        #self.fields['delivery_date'].widget = forms.HiddenInput()
        self.fields['delivery'].required = True

    def clean_delivery_date(self):
        delivery_date = self.cleaned_data['delivery_date']
        if not delivery_date:
            return delivery_date
        delivery_date_now = datetime.datetime.now() + datetime.timedelta(hours=1)
        if delivery_date <= delivery_date_now:
            raise forms.ValidationError(
                _ul(u'Дата доставки должна быть больше %s' % (
                    delivery_date_now.strftime('%d.%m.%Y %H:%M'),
                    ))
                )
        return delivery_date

    class Meta:
        model = Order
        fields = ('first_name', 'last_name', 'phone', 'email', 'delivery', 'delivery_date', 'note')


class AssistPaymentForm(forms.Form):
    Merchant_ID = forms.IntegerField()
    OrderNumber = forms.CharField(max_length=128)
    OrderAmount = forms.DecimalField(max_digits=17, decimal_places=0)
    OrderCurrency = forms.CharField(max_length=3, initial="RUB")
    FirstName = forms.CharField(max_length=70)
    LastName = forms.CharField(max_length=70)
    Email = forms.CharField(max_length=128)
    OrderComment = forms.CharField(max_length=256)
    MobilePhone = forms.CharField(max_length=20)

    CardPayment = forms.IntegerField()
    YMPayment = forms.IntegerField()
    WMPayment = forms.IntegerField()
    QIWIPayment = forms.IntegerField()
    QIWIMtsPayment = forms.IntegerField()
    QIWIMegafonPayment = forms.IntegerField()
    QIWIBeelinePayment = forms.IntegerField()

    URL_RETURN_OK = forms.CharField(max_length=255)
    URL_RETURN_NO = forms.CharField(max_length=255)

    def __init__(self, *args, **kwargs):
        super(AssistPaymentForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget = forms.HiddenInput()
