#-*- encoding: utf-8 -*-
from decimal import Decimal, ROUND_DOWN, ROUND_HALF_DOWN
import requests
import datetime

from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View, TemplateView, ListView, DetailView, CreateView, RedirectView
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils import timezone
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import render_to_string

from users.models import FamilyMember
from main.models import Page
from main.mixins import SeoMixin
from main.decorators import login_required
from shop.models import Catalog, Item, Order, Discount, OrderItem
from shop.forms import OrderForm, AssistPaymentForm
from users.models import User


class ItemList(SeoMixin, ListView):
    template_name = 'main/items.html'
    paginate_by = 10
    model = Item

    def get_queryset(self):
        qs = super(ItemList, self).get_queryset()
        qs = qs.filter(is_published=True, catalog__is_published=True)
        qs = qs.filter(catalog=self.catalog).order_by('-creation_date').order_by('sorting')
        return qs

    def get_context_data(self, **kwargs):
        kwargs = super(ItemList, self).get_context_data(**kwargs)
        if self.page is None:
            kwargs['object'] = self.catalog
        else:
            kwargs['object'] = self.page
        kwargs['catalogs'] = Catalog.objects.filter(is_published=True).order_by('sorting')
        kwargs['hide_presentation'] = self.hide_presentation
        kwargs = self.get_context_seo_data(**kwargs)
        return kwargs

    def dispatch(self, request, *args, **kwargs):
        slug = kwargs.get('slug', None)
        if slug is None:
            self.catalog = Catalog.objects.filter(is_published=True).first()
            self.page = Page.objects.filter(url=request.path, is_published=True).first()
            self.hide_presentation = False
        else:
            self.hide_presentation = True
            self.catalog = get_object_or_404(Catalog, slug=slug, is_published=True)
            self.page = None
        return super(ItemList, self).dispatch(request, *args, **kwargs)


class ItemDetail(SeoMixin, DetailView):
    template_name = 'main/item.html'
    model = Item

    def get_object(self, queryset=None):
        qs = self.model.objects.all()
        qs = qs.filter(is_published=True, catalog__is_published=True)
        return super(ItemDetail, self).get_object(queryset=qs)

    def get_context_data(self, **kwargs):
        kwargs = super(ItemDetail, self).get_context_data(**kwargs)
        kwargs = self.get_context_seo_data(**kwargs)
        kwargs['catalogs'] = Catalog.objects.filter(is_published=True).order_by('sorting')
        kwargs['similar_items'] = self.model.objects.filter(catalog=self.object.catalog).exclude(pk=self.object.pk)
        return kwargs


def get_discount(request, delivery_date=None):
    discounts = []
    if request.user.is_authenticated():
        d_obj = Discount.objects.filter(active=True, action=Discount.DISCOUNT_ACTION_AUTH).first()
        if d_obj:
            discounts.append(d_obj)
    if delivery_date is not None:
        for family in FamilyMember.objects.filter(user=request.user):
            birthday_day = family.m_birthday.day
            birthday_month = family.m_birthday.month
            if family.m_birthday.day == delivery_date.day and family.m_birthday.month == delivery_date.month:
                d_obj = Discount.objects.filter(active=True, action=Discount.DISCOUNT_ACTION_FAMILY_BIRTHDAY).first()
                if d_obj:
                    discounts.append(d_obj)
                break
        delivery_date_plan = datetime.datetime.now() + datetime.timedelta(days=3)
        if delivery_date > delivery_date_plan:
            d_obj = Discount.objects.filter(active=True, action=Discount.DISCOUNT_ACTION_PRE_ORDER).first()
            if d_obj:
                discounts.append(d_obj)
    return sum([d.amount for d in discounts]), [d.action for d in discounts]


def get_item_price(items, obj_pk):
    items = [item for item in items if str(item['item']) == obj_pk]
    if len(items) == 1:
        return items[0]['item_price']
    return 0


def get_cart_kwargs(request, is_dict=False, delivery_date=None, **kwargs):
    kwargs['cart'] = {}
    kwargs['cart']['items'] = OrderItem.objects.get_cart(request, is_dict=is_dict)
    if is_dict:
        kwargs['cart']['items_count'] = sum([item['count'] for item in kwargs['cart']['items']])
        kwargs['cart']['items_price'] = sum([item['item_price'] * Decimal(item['count']) for item in kwargs['cart']['items']])
    else:
        kwargs['cart']['items_count'] = sum([item.count for item in kwargs['cart']['items']])
        kwargs['cart']['items_price'] = sum([item.item_price * Decimal(item.count) for item in kwargs['cart']['items']])
    kwargs['cart']['items_discount'], kwargs['cart']['discounts'] = get_discount(request, delivery_date=delivery_date)
    kwargs['cart']['items_price_with_discount'] = kwargs['cart']['items_price'] - kwargs['cart']['items_price'] * (Decimal(kwargs['cart']['items_discount'])/Decimal(100.0))
    kwargs['cart']['items_price_with_discount'] = kwargs['cart']['items_price_with_discount'].quantize(Decimal('1.'), rounding=ROUND_HALF_DOWN)
    return kwargs


class AjaxView(View):

    def perform(self, post_data):
        return {}

    def post(self, request, *args, **kwargs):
        self.request = request
        data = self.perform(request.POST)
        return JsonResponse(data)

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AjaxView, self).dispatch(*args, **kwargs)


class CartAdd(AjaxView):

    def perform(self, post_data):
        item = post_data.get('item', None)
        if item is not None and item.isdigit() and Item.objects.filter(pk=item).exists():
            obj_pk = str(item)
            cart = self.request.session.get('cart', {})
            if obj_pk not in cart.keys():
                cart[obj_pk] = 1
                self.request.session['cart'] = cart
                cart_obj = get_cart_kwargs(self.request, is_dict=True)
                return {'success': True, 'obj_pk': obj_pk, 'cart_obj': cart_obj}
        return {'success': False}


class CartDelete(AjaxView):

    def perform(self, post_data):
        item = post_data.get('item', None)
        if item is not None and item.isdigit() and Item.objects.filter(pk=item).exists():
            obj_pk = str(item)
            cart = self.request.session.get('cart', {})
            if obj_pk in cart.keys():
                cart.pop(obj_pk, None)
                self.request.session['cart'] = cart
                cart_obj = get_cart_kwargs(self.request, is_dict=True)
                return {'success': True, 'obj_pk': obj_pk, 'cart_obj': cart_obj}
        return {'success': False}


class CartPlus(AjaxView):

    def perform(self, post_data):
        item = post_data.get('item', None)
        if item is not None and item.isdigit() and Item.objects.filter(pk=item).exists():
            obj_pk = str(item)
            cart = self.request.session.get('cart', {})
            if obj_pk in cart.keys():
                cart[obj_pk] += 1
                self.request.session['cart'] = cart
                cart_obj = get_cart_kwargs(self.request, is_dict=True)
                item_count = cart.get(obj_pk, 1)
                item_price = get_item_price(cart_obj['cart']['items'], obj_pk) * item_count
                return {'success': True, 'obj_pk': obj_pk, 'cart_obj': cart_obj, 'item_count': item_count, 'item_price': item_price}
        return {'success': False}


class CartMinus(AjaxView):

    def perform(self, post_data):
        item = post_data.get('item', None)
        if item is not None and item.isdigit() and Item.objects.filter(pk=item).exists():
            obj_pk = str(item)
            cart = self.request.session.get('cart', {})
            if obj_pk in cart.keys():
                if cart[obj_pk] > 1:
                    cart[obj_pk] -= 1
                self.request.session['cart'] = cart
                cart_obj = get_cart_kwargs(self.request, is_dict=True)
                item_count = cart.get(obj_pk, 1)
                item_price = get_item_price(cart_obj['cart']['items'], obj_pk) * item_count
                return {'success': True, 'obj_pk': obj_pk, 'cart_obj': cart_obj, 'item_count': item_count, 'item_price': item_price}
        return {'success': False}


class CartDiscounts(AjaxView):

    def perform(self, post_data):
        delivery_date = post_data.get('delivery_date', None)
        if delivery_date is not None:
            try:
                delivery_date = datetime.datetime.strptime(delivery_date, "%d.%m.%y %H:%M")
                cart_obj = get_cart_kwargs(self.request, delivery_date=delivery_date, is_dict=True)
                is_pre_order_discount = Discount.DISCOUNT_ACTION_PRE_ORDER in cart_obj['cart']['discounts']
                if is_pre_order_discount:
                    discount = Discount.objects.filter(active=True, action=Discount.DISCOUNT_ACTION_PRE_ORDER).first()
                    if discount:
                        discount_msg = u'<br>Ваша дополнительная скидка за заблаговременное бронирование заказа - %s%%!<br>' % discount.amount
                        return {
                            'success': True,
                            'cart_obj': cart_obj,
                            'discount_msg': discount_msg,
                            'cart_table_html': render_to_string('main/order-create-cart.html', {'cart': cart_obj['cart']})
                        }
                else:
                    return {
                        'success': True,
                        'cart_obj': cart_obj,
                        'discount_msg': None,
                        'cart_table_html': render_to_string('main/order-create-cart.html', {'cart': cart_obj['cart']})
                    }
            except ValueError:
                pass
        return {'success': False}


class CartDetail(TemplateView):
    template_name = 'main/cart.html'

    def get_context_data(self, **kwargs):
        kwargs = super(CartDetail, self).get_context_data(**kwargs)
        kwargs['cart_text'] = u'Вы можете воспользоваться бесплатной доставкой по г. Казани, либо забрать заказ по адресу:  ул. Четаева, 25, Салон флористики «Бутон».'
        return get_cart_kwargs(self.request, **kwargs)


class BuyOneClick(RedirectView):
    permanent = False

    def get_redirect_url(self):
        item = self.request.GET.get('item', None)
        if item is not None and item.isdigit() and Item.objects.filter(pk=item).exists():
            obj_pk = str(item)
            cart = {}
            cart[obj_pk] = 1
            self.request.session['cart'] = cart
        return reverse_lazy('order-create')


def get_delivery_date():
    date = datetime.datetime.now()
    hour = int(date.strftime('%H'))
    if hour >= 0 and hour < 8:
        date = date.replace(hour=10)
    if hour >= 8 and hour <= 17:
        date = date + datetime.timedelta(hours=2)
    if hour > 17 and hour <= 23:
        date = date + datetime.timedelta(days=1)
        date = date.replace(hour=10)
    date = date.replace(minute=0, second=0, microsecond=0)
    return date


class OrderCreate(CreateView):
    template_name = 'main/order-create.html'
    model = Order
    form_class = OrderForm
    success_url = reverse_lazy('index')

    def get_initial(self):
        initial = super(OrderCreate, self).get_initial()
        if self.request.user.is_authenticated():
            initial['first_name'] = self.request.user.first_name
            initial['last_name'] = self.request.user.last_name
            initial['phone'] = self.request.user.phone
            initial['email'] = self.request.user.email
            initial['delivery'] = self.request.user.delivery
        initial['delivery_date'] = get_delivery_date()
        return initial

    def get_context_data(self, **kwargs):
        kwargs = super(OrderCreate, self).get_context_data(**kwargs)
        kwargs['discount_auth'] = Discount.objects.filter(active=True, action=Discount.DISCOUNT_ACTION_AUTH).first()
        delivery_date = None
        if hasattr(kwargs['form'], 'cleaned_data'):
            delivery_date = kwargs['form'].cleaned_data.get('delivery_date')
        if not delivery_date:
            delivery_date = get_delivery_date()
        return get_cart_kwargs(self.request, delivery_date=delivery_date, **kwargs)

    def form_valid(self, form):
        order = Order()
        if self.request.user.is_authenticated():
            order.user = self.request.user
        order.first_name = form.cleaned_data.get('first_name')
        order.last_name = form.cleaned_data.get('last_name')
        order.phone = form.cleaned_data.get('phone')
        order.email = form.cleaned_data.get('email')
        order.delivery = form.cleaned_data.get('delivery')
        order.delivery_date = form.cleaned_data.get('delivery_date')
        order.note = form.cleaned_data.get('note')
        order.delivery_type = int(self.request.POST.get('delivery_type', Order.DELIVERY_SELF))
        order.payment_type = int(self.request.POST.get('payment_type', Order.PAYMENT_CASH))
        order.status = Order.ORDER_STATUS_NEW

        cart_kwargs = get_cart_kwargs(self.request, delivery_date=order.delivery_date)

        is_pre_order_discount = Discount.DISCOUNT_ACTION_PRE_ORDER in cart_kwargs['cart']['discounts']
        order.items_count = cart_kwargs['cart']['items_count']
        order.items_price = cart_kwargs['cart']['items_price']
        order.items_discount = cart_kwargs['cart']['items_discount']
        order.items_price_with_discount = cart_kwargs['cart']['items_price_with_discount']
        order.save()

        if self.request.user.is_authenticated():
            is_changed = False
            if not self.request.user.first_name:
                self.request.user.first_name = order.first_name
                is_changed = True
            if not self.request.user.last_name:
                self.request.user.last_name = order.last_name
                is_changed = True
            if not self.request.user.delivery:
                self.request.user.delivery = order.delivery
                is_changed = True
            if not self.request.user.phone:
                self.request.user.phone = order.phone
                is_changed = True
            if is_changed:
                self.request.user.save()

        for item in cart_kwargs['cart']['items']:
            item.order = order
            item.save()

        User.objects.create_order(get_current_site(self.request), order)

        self.request.session['cart'] = {}
        if order.payment_type in (Order.PAYMENT_CREDIT_CARD, Order.PAYMENT_ECOMMERCE):
            return redirect(reverse_lazy('order-payment', kwargs={'pk': order.pk}))
        success_msg = u'Заказ успешно оформлен.'
        if not self.request.user.is_authenticated():
            discount = Discount.objects.filter(active=True, action=Discount.DISCOUNT_ACTION_AUTH).first()
            if discount:
                success_msg += u'<br><br>После авторизации вы получите скидку %s%% на все товары нашего магазина!<br>' % discount.amount
        if is_pre_order_discount:
            discount = Discount.objects.filter(active=True, action=Discount.DISCOUNT_ACTION_PRE_ORDER).first()
            if discount:
                success_msg += u'<br><br>Ваша дополнительная скидка за заблаговременное бронирование заказа - %s%%!<br>' % discount.amount
        messages.success(self.request, success_msg)
        return redirect(self.success_url)


class OrderPayment(DetailView):
    template_name = 'main/order-payment.html'
    model = Order

    def get_context_data(self, **kwargs):
        kwargs = super(OrderPayment, self).get_context_data(**kwargs)
        site = get_current_site(self.request)
        site_profile = site.get_profile()
        order = kwargs['object']
        initial = {
            'Merchant_ID': site_profile.assist_merchant_id,
            'OrderNumber': order.id,
            'OrderAmount': order.items_price_with_discount,
            'FirstName': order.first_name,
            'LastName': order.last_name,
            'Email': order.email,
            'MobilePhone': order.phone,
            'OrderComment': order.note or u'',
            'URL_RETURN_OK': site_profile.http_methog + site.domain + reverse('order-payment-ok'),
            'URL_RETURN_NO': site_profile.http_methog + site.domain + reverse('order-payment-no'),
        }
        if order.payment_type == Order.PAYMENT_CREDIT_CARD:
            initial['CardPayment'] = 1
            initial['YMPayment'] = 0
            initial['WMPayment'] = 0
            initial['QIWIPayment'] = 0
            initial['QIWIMtsPayment'] = 0
            initial['QIWIMegafonPayment'] = 0
            initial['QIWIBeelinePayment'] = 0
        if order.payment_type == Order.PAYMENT_ECOMMERCE:
            initial['CardPayment'] = 0
            initial['YMPayment'] = 1
            initial['WMPayment'] = 1
            initial['QIWIPayment'] = 1
            initial['QIWIMtsPayment'] = 1
            initial['QIWIMegafonPayment'] = 1
            initial['QIWIBeelinePayment'] = 1
        kwargs['form'] = AssistPaymentForm(initial=initial)
        kwargs['assist_url'] = site_profile.assist_url
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data()
        form = context['form']
        assist_url = context['assist_url']
        headers = {'User-Agent': 'Mozilla/5.0'}
        session = requests.Session()
        form_data = form.initial
        form_data['MobilePhone'] = str(form_data.get('MobilePhone',''))
        form_data['OrderAmount'] = str(form_data.get('OrderAmount',''))
        result =  session.post(assist_url, data=form_data)
        return redirect(result.url)

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OrderPayment, self).dispatch(*args, **kwargs)


class OrderPaymentOk(RedirectView):
    permanent = False

    def get_redirect_url(self):
        billnumber = self.request.GET.get('billnumber', None)
        ordernumber = self.request.GET.get('ordernumber')
        Order.objects.filter(pk=ordernumber).update(status=Order.ORDER_STATUS_PAYMENT)
        messages.success(self.request, u'Оплата прошла успешно.')
        return reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OrderPaymentOk, self).dispatch(*args, **kwargs)


class OrderPaymentNo(RedirectView):
    permanent = False

    def get_redirect_url(self):
        billnumber = self.request.GET.get('billnumber', None)
        ordernumber = self.request.GET.get('ordernumber')
        messages.success(self.request, u'Возникли ошибки при оплате. Попробуйте повторить Ваш платеж.')
        return reverse_lazy('order-detail', kwargs={'pk': ordernumber})

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OrderPaymentNo, self).dispatch(*args, **kwargs)


class OrderList(ListView):
    model = Order
    template_name = 'main/orders.html'

    def get_queryset(self):
        qs = super(OrderList, self).get_queryset()
        qs = qs.filter(user=self.request.user)
        return qs.order_by('-modification_date')

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(OrderList, self).dispatch(*args, **kwargs)


class OrderDetail(DetailView):
    model = Order
    template_name = 'main/order-detail.html'