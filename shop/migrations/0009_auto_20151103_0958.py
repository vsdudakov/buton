# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0008_auto_20151022_0858'),
    ]

    operations = [
        migrations.AlterField(
            model_name='catalog',
            name='description',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='item',
            name='description',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='delivery',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='note',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='item_description',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
