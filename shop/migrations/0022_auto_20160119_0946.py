# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0021_auto_20151221_2220'),
    ]

    operations = [
        migrations.AddField(
            model_name='itemimage',
            name='alt',
            field=models.CharField(max_length=255, null=True, verbose_name='Alt \u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='last_name',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f', blank=True),
        ),
    ]
