# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0007_auto_20151018_2301'),
    ]

    operations = [
        migrations.AddField(
            model_name='catalog',
            name='sorting',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430'),
        ),
        migrations.AddField(
            model_name='item',
            name='sorting',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430'),
        ),
    ]
