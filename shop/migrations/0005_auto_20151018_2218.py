# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('shop', '0004_auto_20151018_1158'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('name', models.CharField(max_length=255, null=True, verbose_name='\u0418\u043c\u044f', blank=True)),
                ('phone', models.CharField(max_length=255, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430')),
                ('email', models.EmailField(max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u044d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u043e\u0439 \u043f\u043e\u0447\u0442\u044b')),
                ('delivery_type', models.PositiveIntegerField(default=0, verbose_name='\u0412\u0430\u0440\u0438\u0430\u043d\u0442 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438', choices=[(0, '\u0421\u0430\u043c\u043e\u0432\u044b\u0432\u043e\u0437'), (1, '\u0414\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u0434\u043e \u0434\u0432\u0435\u0440\u0438')])),
                ('delivery', ckeditor.fields.RichTextField(null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438', blank=True)),
                ('payment_type', models.PositiveIntegerField(default=0, verbose_name='\u0412\u0430\u0440\u0438\u0430\u043d\u0442 \u043e\u043f\u043b\u0430\u0442\u044b', choices=[(0, '\u041d\u0430\u043b\u0438\u0447\u043d\u044b\u0435'), (1, '\u0411\u0430\u043d\u043a\u043e\u0432\u0441\u043a\u0430\u044f \u043a\u0430\u0440\u0442\u0430'), (2, '\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u044b\u0435 \u0434\u0435\u043d\u044c\u0433\u0438')])),
                ('note', ckeditor.fields.RichTextField(null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0438', blank=True)),
                ('status', models.PositiveIntegerField(default=0, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(0, '\u041d\u043e\u0432\u044b\u0439'), (1, '\u0417\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u043d\u044b\u0439')])),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437',
            },
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('item_title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('item_description', ckeditor.fields.RichTextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('item_price', models.DecimalField(default=0.0, verbose_name='\u0426\u0435\u043d\u0430', max_digits=10, decimal_places=2)),
                ('count', models.PositiveIntegerField(default=1, verbose_name='\u041a\u043e\u043b-\u0432\u043e')),
                ('item', models.ForeignKey(verbose_name='\u0422\u043e\u0432\u0430\u0440', to='shop.Item')),
                ('item_catalog', models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0430\u043b\u043e\u0433', to='shop.Catalog')),
                ('order', models.ForeignKey(verbose_name='\u0417\u0430\u043a\u0430\u0437', to='shop.Order')),
            ],
            options={
                'verbose_name': '\u0422\u043e\u0432\u0430\u0440 \u0437\u0430\u043a\u0430\u0437\u0430',
                'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b \u0437\u0430\u043a\u0430\u0437\u0430',
            },
        ),
        migrations.AddField(
            model_name='order',
            name='items',
            field=models.ManyToManyField(to='shop.Item', verbose_name='\u0422\u043e\u0432\u0430\u0440\u044b', through='shop.OrderItem', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='user',
            field=models.ForeignKey(verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='orderitem',
            unique_together=set([('order', 'item')]),
        ),
    ]
