# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0020_auto_20151220_2106'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discount',
            name='amount',
            field=models.DecimalField(default=0.0, verbose_name='\u0421\u043a\u0438\u0434\u043a\u0430 \u0432 %', max_digits=10, decimal_places=0),
        ),
        migrations.AlterField(
            model_name='item',
            name='price',
            field=models.DecimalField(default=0.0, verbose_name='\u0426\u0435\u043d\u0430', max_digits=10, decimal_places=0),
        ),
        migrations.AlterField(
            model_name='order',
            name='items_discount',
            field=models.DecimalField(null=True, verbose_name='\u0421\u043a\u0438\u0434\u043a\u0430 \u0432 %', max_digits=10, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='items_price',
            field=models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430', max_digits=10, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='items_price_with_discount',
            field=models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430 c\u043e \u0441\u043a\u0438\u0434\u043a\u043e\u0439', max_digits=10, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='item_price',
            field=models.DecimalField(default=0.0, verbose_name='\u0426\u0435\u043d\u0430', max_digits=10, decimal_places=0),
        ),
    ]
