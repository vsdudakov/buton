# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields
import shop.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Catalog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('is_published', models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d?')),
                ('slug', models.SlugField(unique=True, max_length=255, verbose_name='\u0421\u043b\u0430\u0433', blank=True)),
                ('title', models.CharField(unique=True, max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('seo_title', models.CharField(max_length=255, null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a (meta \u0442\u0435\u0433)', blank=True)),
                ('seo_keywords', models.CharField(max_length=255, null=True, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 (meta \u0442\u0435\u0433)', blank=True)),
                ('seo_description', models.CharField(max_length=255, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 (meta \u0442\u0435\u0433)', blank=True)),
                ('seo_author', models.CharField(max_length=255, null=True, verbose_name='\u0410\u0432\u0442\u043e\u0440 (meta \u0442\u0435\u0433)', blank=True)),
                ('description', ckeditor.fields.RichTextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0442\u0430\u043b\u043e\u0433',
                'verbose_name_plural': '\u041a\u0430\u0442\u0430\u043b\u043e\u0433\u0438',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('is_published', models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d?')),
                ('slug', models.SlugField(unique=True, max_length=255, verbose_name='\u0421\u043b\u0430\u0433', blank=True)),
                ('title', models.CharField(unique=True, max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('seo_title', models.CharField(max_length=255, null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a (meta \u0442\u0435\u0433)', blank=True)),
                ('seo_keywords', models.CharField(max_length=255, null=True, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 (meta \u0442\u0435\u0433)', blank=True)),
                ('seo_description', models.CharField(max_length=255, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 (meta \u0442\u0435\u0433)', blank=True)),
                ('seo_author', models.CharField(max_length=255, null=True, verbose_name='\u0410\u0432\u0442\u043e\u0440 (meta \u0442\u0435\u0433)', blank=True)),
                ('description', ckeditor.fields.RichTextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('price', models.DecimalField(default=0.0, verbose_name='\u0426\u0435\u043d\u0430', max_digits=10, decimal_places=2)),
                ('catalog', models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0430\u043b\u043e\u0433', to='shop.Catalog')),
            ],
            options={
                'verbose_name': '\u0422\u043e\u0432\u0430\u0440',
                'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='ItemImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('is_main', models.BooleanField(default=False, verbose_name='\u0413\u043b\u0430\u0432\u043d\u0430\u044f?')),
                ('img', models.ImageField(upload_to=shop.models.img_upload_to, verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430')),
                ('item', models.ForeignKey(verbose_name='\u0422\u043e\u0432\u0430\u0440', to='shop.Item')),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430 \u0442\u043e\u0432\u0430\u0440\u0430',
                'verbose_name_plural': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0438 \u0442\u043e\u0432\u0430\u0440\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='ItemPart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('count', models.PositiveIntegerField(default=0, verbose_name='\u041a\u043e\u043b-\u0432\u043e')),
                ('item', models.ForeignKey(verbose_name='\u0422\u043e\u0432\u0430\u0440', to='shop.Item')),
            ],
            options={
                'verbose_name': '\u0421\u043e\u0441\u0442\u0430\u0432 \u0442\u043e\u0432\u0430\u0440\u0430',
                'verbose_name_plural': '\u0421\u043e\u0441\u0442\u0430\u0432 \u0442\u043e\u0432\u0430\u0440\u043e\u0432',
            },
        ),
    ]
