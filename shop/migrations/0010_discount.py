# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0009_auto_20151103_0958'),
    ]

    operations = [
        migrations.CreateModel(
            name='Discount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('action', models.PositiveIntegerField(verbose_name='\u0412\u0430\u0440\u0438\u0430\u043d\u0442 \u0441\u043a\u0438\u0434\u043a\u0438', choices=[(0, '\u0421\u043a\u0438\u0434\u043a\u0430 \u0434\u043b\u044f \u0430\u0432\u0442\u043e\u0440\u0438\u0437\u043e\u0432\u0430\u043d\u043d\u043e\u0433\u043e \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f'), (1, '\u0421\u043a\u0438\u0434\u043a\u0430 \u0432 \u0434\u0435\u043d\u044c \u0440\u043e\u0436\u0434\u0435\u043d\u0438\u044f \u0447\u043b\u0435\u043d\u0430 \u0441\u0435\u043c\u044c\u0438')])),
                ('discount_type', models.PositiveIntegerField(default=0, verbose_name='\u0422\u0438\u043f \u0441\u043a\u0438\u0434\u043a\u0438', choices=[(0, '\u0421\u043a\u0438\u0434\u043a\u0430 \u0432 \u043f\u0440\u043e\u0446\u0435\u043d\u0442\u0430\u0445'), (1, '\u0421\u043a\u0438\u0434\u043a\u0430 \u0432 \u0440\u0443\u0431\u043b\u044f\u0445')])),
                ('amount', models.DecimalField(default=0.0, verbose_name='\u0426\u0435\u043d\u0430', max_digits=10, decimal_places=2)),
                ('active', models.BooleanField(default=False, verbose_name='\u0410\u043a\u0442\u0438\u0432\u0438\u0440\u043e\u0432\u0430\u043d\u0430?')),
            ],
            options={
                'verbose_name': '\u0421\u043a\u0438\u0434\u043a\u0430',
                'verbose_name_plural': '\u0421\u043a\u0438\u0434\u043a\u0438',
            },
        ),
    ]
