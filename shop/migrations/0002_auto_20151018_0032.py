# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='itemimage',
            options={'verbose_name': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430 \u0442\u043e\u0432\u0430\u0440\u0430', 'verbose_name_plural': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0438 \u0442\u043e\u0432\u0430\u0440\u0430'},
        ),
        migrations.AlterModelOptions(
            name='itempart',
            options={'verbose_name': '\u0421\u043e\u0441\u0442\u0430\u0432 \u0442\u043e\u0432\u0430\u0440\u0430', 'verbose_name_plural': '\u0421\u043e\u0441\u0442\u0430\u0432 \u0442\u043e\u0432\u0430\u0440\u0430'},
        ),
    ]
