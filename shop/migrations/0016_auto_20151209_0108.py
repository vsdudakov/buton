# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0015_auto_20151204_1630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(0, '\u041d\u043e\u0432\u044b\u0439'), (1, '\u041e\u043f\u043b\u0430\u0447\u0435\u043d\u043d\u044b\u0439'), (2, '\u0417\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u043d\u044b\u0439')]),
        ),
    ]
