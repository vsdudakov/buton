$(document).ready(function() {
     // $(".cart-li-add").click(function(){
     //     $(".cart-li-add")
     //      .clone()
     //      .css({'position' : 'absolute', 'z-index' : '100'})
     //      .appendTo(".cart-li-add")
     //      .animate({opacity: 0.5,
     //                    marginLeft: 700,
     //                    width: 50,
     //                    height: 50}, 700, function() {
     //            $(this).remove();
     //      });
     // });
    var addCart = function(item){
        $.ajax({
            type: 'POST',
            url: cartAddUrl,
            data: {
                'item': item
            },
            success: function(msg){
              if (msg.success) {
                  $('#cart-count').html(msg.cart_obj.cart.items_count);
              }
            }
        });
    };
    var deleteCart = function(item){
        $.ajax({
            type: 'POST',
            url: cartDeleteUrl,
            data: {
                'item': item
            },
            success: function(msg){
              if (msg.success) {
                  $('#cart-count').html(msg.cart_obj.cart.items_count);
                  $('.cart-item-' + msg.obj_pk).remove();
                  if (msg.cart_obj.cart.items_count == 0) {
                      $('#cart-payment').remove();
                  }
                  $('#cart-items-count').html(msg.cart_obj.cart.items_count);
                  $('#cart-items-price').html(msg.cart_obj.cart.items_price);
                  $('#cart-items-discount').html(msg.cart_obj.cart.items_discount);
                  $('#cart-items-price-with-discount').html(msg.cart_obj.cart.items_price_with_discount);
              }
            }
        });
    };
    var plusCart = function(item){
        $.ajax({
            type: 'POST',
            url: cartPlusUrl,
            data: {
                'item': item
            },
            success: function(msg){
              if (msg.success) {
                  $('.cart-item-count-' + msg.obj_pk).val(msg.item_count);
                  $('.cart-item-price-' + msg.obj_pk).html(msg.item_price);
                  $('#cart-items-count').html(msg.cart_obj.cart.items_count);
                  $('#cart-items-price').html(msg.cart_obj.cart.items_price);
                  $('#cart-items-discount').html(msg.cart_obj.cart.items_discount);
                  $('#cart-items-price-with-discount').html(msg.cart_obj.cart.items_price_with_discount);
              }
            }
        });
    };
    var minusCart = function(item){
        $.ajax({
            type: 'POST',
            url: cartMinusUrl,
            data: {
                'item': item
            },
            success: function(msg){
              if (msg.success) {
                  $('.cart-item-count-' + msg.obj_pk).val(msg.item_count);
                  $('.cart-item-price-' + msg.obj_pk).html(msg.item_price);
                  $('#cart-items-count').html(msg.cart_obj.cart.items_count);
                  $('#cart-items-price').html(msg.cart_obj.cart.items_price);
                  $('#cart-items-discount').html(msg.cart_obj.cart.items_discount);
                  $('#cart-items-price-with-discount').html(msg.cart_obj.cart.items_price_with_discount);
              }
            }
        });
    };
    var discountsCart = function(delivery_date){
        $.ajax({
            type: 'POST',
            url: cartDiscountsUrl,
            data: {
                'delivery_date': delivery_date
            },
            success: function(msg){
              if (msg.success) {
                  $('#table-cart').html(msg.cart_table_html);
                  if (msg.discount_msg) {
                    $('#msg .msg-data').html(msg.discount_msg);
                    $('#msg').modal('show');
                  }
              }
            }
        });
    };

    $('.cart-add').click(function(){
        addCart($(this).attr('data'));
    });
    $('.cart-plus').click(function(){
        plusCart($(this).attr('data'));
    });
    $('.cart-minus').click(function(){
        minusCart($(this).attr('data'));
    });
    $('.cart-delete').click(function(){
        deleteCart($(this).attr('data'));
    });
    $('.cart-discounts').change(function(){
        discountsCart($(this).val());
    });
});
