$(document).ready(function() { 
    var changeMember = function(item, name, birthday){
      $('#member form #id_name').val(name);
      $('#member form #id_m_birthday').val(birthday);
      $('#member form').attr('action', changeMemberUrl.replace('0', item));
      $('#member').modal('show');
    };
    var addMember = function(){
      $('#member form #id_name').val('');
      $('#member form #id_m_birthday').val('');
      $('#member form').attr('action', addMemberUrl);
      $('#member').modal('show')
    };

    $('.change-member').click(function(){
        changeMember($(this).attr('data'), $(this).attr('data-name'), $(this).attr('data-birthday'));
    });
    $('.add-member').click(function(){
        addMember();
    });
});
	