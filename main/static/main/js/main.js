		/* $(document).ready(function() { */
		$(window).load(function() {
			var win_w = $(window).width();
			
			$('.show-menu').click(function(){
				$('.top_right__menu').slideToggle();
			});
			
			$('.positem_infoholder__gallery').each(function(i){
				var elemID = $(this).attr('id');
				var thisposTitle = $(this).parents('.positem_infoholder').children('.positem_infoholder__title').text();
				
					$('#' + elemID).owlCarousel({
						items: 3,
						itemsDesktop: [1000,3],
						itemsDesktopSmall: [900,3],
						itemsTablet: [600,2],
						itemsMobile: [400,1]
					 }); 
					 
					$('#' + elemID + '-next').click(function(){
						$('#' + elemID).trigger('owl.next');
					});
					
					$('#' + elemID + '-prev').click(function(){
						$('#' + elemID).trigger('owl.prev');
					});
					
					$('.fresco',this).attr('data-fresco-caption',thisposTitle);
					$('.fresco',this).attr('data-fresco-group',elemID);
			});
			
			if (win_w >= 992) {
				$('.positem-inner').each(function(i){
					var elemHeight = $('.positem_infoholder',this).innerHeight();
					
					$('.positem_imgholder',this).css('height',elemHeight);
				});
				
				$('.positem_imgholder__link').each(function(i){
					var elemWidth = $(this).innerWidth();
					var elemOffset = elemWidth / 2;
					
					$(this).css('margin-left',-elemOffset);
				});
				
				$('.positem:nth-child(odd)').each(function(i) {
					$(this).next('.positem').andSelf().wrapAll('<div class="positem-holder"></div>')
				});
			};
			
			if (win_w > 991) {
				$('.article.col-md-4:nth-child(3n)').after('<div class="clearfix"></div>');
			} else {
				$('.article.col-md-4:nth-child(2n)').after('<div class="clearfix"></div>');
			};
			$('.cartbox_item:nth-child(2n)').after('<div class="clearfix"></div>');
			
			var headerHeight = $('.header').height();
			var ccontrolOffset = headerHeight / 2;
			$('.header .carousel-control').css('margin-top',-ccontrolOffset);
			
			var cindicWidth = $('.carousel-indicators').innerWidth();
			var cindic = cindicWidth / 2;
			$('.header .carousel-indicators').css('margin-left',-cindic);
			
			/*var footerH = $('.footer').innerHeight();			
			$('.pre-footer').css('min-height',footerH);*/
			
			var docH = $(document).innerHeight();
			var winH = $(window).innerHeight();			
			if (docH > winH) {
				$('body').addClass('pos-rel');
			};
			$('#id_birthday').datetimepicker({pickTime: false, language: 'ru'});
			$('#id_m_birthday').datetimepicker({pickTime: false, language: 'ru'});
		});


		CALENDAR.bind(
    		document.getElementById( "id_delivery_date" ),
    		{
    			y1:	true,
    			showTime: true
    		}
    	);
		// $('#id_delivery_date_widget').attr('readonly', true);
		// $('#id_delivery_date_widget').datetimepicker(
		// 	{
		// 		pickTime: true, 
		// 		minDate: moment(),
		// 		language: 'ru',
		// 		ignoreReadonly: true
		// 	}
		// ).on('changeDate', function (ev) {
		//     $('#id_delivery_date_widget').change();
		// });
		// $('#id_delivery_date_widget').change(function(){
		// 	$('#id_delivery_date').val($('#id_delivery_date_widget').val());
		// });
		
		$(window).resize(function(){
			var footerHeight = $('.footer').innerHeight();	
			
			$('.pre-footer').css('height',footerHeight);
		});
				
		$(window).scroll(function() {
			var topH = $('.top').innerHeight();	
		
			if ($(this).scrollTop() > topH) {
				$('.i-nav').parent().addClass('i-panel');
				$('.i-nav').addClass('container');
			} else {
				$('.i-nav').parent().removeClass('i-panel');
				$('.i-nav').removeClass('container');
			}
		});		