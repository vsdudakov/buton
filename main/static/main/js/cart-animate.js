
$(document).ready(function(){
    $(".cart-add").click(function(){
        var idItem = $(this).attr('data');
        var item = $('#item-' + idItem);
        var itemOffset = item.offset();
        var cartOffset = $('#cart-count').offset();
        var cloneItem = item
            .clone()
            .css({
                'position' : 'absolute', 
                'z-index' : '10000', 
                'top': itemOffset.top, 
                'left': itemOffset.left,
                'width': item.css('width'),
                'height': item.css('height')
            })  
            .appendTo('body'); 
        cloneItem.animate({
            opacity: 0.5, 
            top: cartOffset.top,
            left : cartOffset.left,
            width: 5, 
            height: 5
            }, 1000, function() {
                $(this).remove();
          });
     });
});
