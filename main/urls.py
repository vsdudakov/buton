"""buton URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from filebrowser.sites import site

from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

from main.views import ContactsView, PageView, CallView, ReviewsView, FeedbackView
from shop.views import (
    ItemList, 
    ItemDetail,
    CartDetail,
    CartAdd,
    CartDelete,
    CartPlus,
    CartMinus,
    CartDiscounts,
    OrderCreate,
    OrderPayment,
    OrderPaymentOk,
    OrderPaymentNo,
    OrderList,
    OrderDetail,
    BuyOneClick,
    )
from articles.views import ArticleList, ArticleDetail
from users.views import (
    LoginView, 
    LogoutView, 
    RegistrationView, 
    RegistrationCompleteView, 
    ResetPasswordView,
    ResetPasswordCompleteView,
    ProfileView,
    ProfileUpdateView,
    ChangePasswordView,
    ProfileMemberCreateView,
    ProfileMemberUpdateView
    )


urlpatterns = [
    url(r'^$', ItemList.as_view(), name='index'),
    url(r'^contacts/$', ContactsView.as_view(), name='contacts'),

    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^registration/$', RegistrationView.as_view(), name='registration'),
    url(r'^registration/(?P<uuid>.*)/$', RegistrationCompleteView.as_view(), name='registration-complete'),
    url(r'^reset/password/$', ResetPasswordView.as_view(), name='reset-password'),
    url(r'^reset/password/(?P<uuid>.*)/$', ResetPasswordCompleteView.as_view(), name='reset-password-complete'),
    url(r'^profile/$', ProfileView.as_view(), name='profile'),
    url(r'^profile/update/$', ProfileUpdateView.as_view(), name='profile-update'),
    url(r'^change/password/$', ChangePasswordView.as_view(), name='change-password'),
    url(r'^profile/member/create/$', ProfileMemberCreateView.as_view(), name='profile-member-create'),
    url(r'^profile/member/update/(?P<pk>[0-9]+)/$', ProfileMemberUpdateView.as_view(), name='profile-member-update'),
    url(r'^reviews/$', ReviewsView.as_view(), name='reviews'),

    url(r'^buy/$', BuyOneClick.as_view(), name='buy-one-click'),
    url(r'^call/$', CallView.as_view(), name='call'),
    url(r'^feedback/$', FeedbackView.as_view(), name='feedback'),

    url(r'^category/$', ArticleList.as_view(), name='articles'),
    url(r'^category/(?P<slug>[-_\w]+)/$', ArticleList.as_view(), name='articles'),
    url(r'^articles/(?P<slug>[-_\w]+)/$', ArticleDetail.as_view(), name='articles-detail'),

    url(r'^catalog/(?P<slug>[-_\w]+)/$', ItemList.as_view(), name='items'),
    url(r'^items/(?P<slug>[-_\w]+)/$', ItemDetail.as_view(), name='items-detail'),
    url(r'^cart/$', CartDetail.as_view(), name='cart-detail'),
    url(r'^order/$', OrderCreate.as_view(), name='order-create'),
    url(r'^order/payment/ok/$', OrderPaymentOk.as_view(), name='order-payment-ok'),
    url(r'^order/payment/no/$', OrderPaymentNo.as_view(), name='order-payment-no'),
    url(r'^order/payment/(?P<pk>[0-9]+)/$', OrderPayment.as_view(), name='order-payment'),
    url(r'^order/list/$', OrderList.as_view(), name='order-list'),
    url(r'^order/detail/(?P<pk>[0-9]+)/$', OrderDetail.as_view(), name='order-detail'),

    url(r'^ajax/cart/plus/$', CartPlus.as_view(), name='ajax-cart-plus'),
    url(r'^ajax/cart/minus/$', CartMinus.as_view(), name='ajax-cart-minus'),
    url(r'^ajax/cart/add/$', CartAdd.as_view(), name='ajax-cart-add'),
    url(r'^ajax/cart/delete/$', CartDelete.as_view(), name='ajax-cart-delete'),
    url(r'^ajax/cart/discounts/$', CartDiscounts.as_view(), name='ajax-cart-discounts'),
    
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^social/', include('social_auth.urls')),

    url(r'^(?i)robots\.txt$', 'main.views_seo.robots_txt', name='robots'),
    url(r'^(?i)sitemap\.xml$', 'main.views_seo.sitemap_xml', name='sitemap_xml'),
    url(r'^(?i)sitemap-(?P<section>.+)\.xml$', 'main.views_seo.sitemap_xml', name='sitemap-section'),
]


if settings.DEBUG:
    urlpatterns += [ 
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    ]


urlpatterns += [   
    url(r'^(?P<url>.*/)$', PageView.as_view(), name='pages'),
]

