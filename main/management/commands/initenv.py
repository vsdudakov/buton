#-*- encoding: utf-8
from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command
from django.conf import settings
from django.utils.translation import ugettext_lazy as _ul
from django.contrib.sites.models import Site


from users.models import User
from main.models import Page, Menu, MenuItem, Carousel, CarouselItem, SiteProfile
from articles.models import Category
from shop.models import Catalog, Discount

FORCE_SYNC = False

class Command(BaseCommand):

    def create_users(self):
        if User.objects.all().exists() and not FORCE_SYNC:
            return
        admin, _ = User.objects.get_or_create(username='admin@admin.com')
        admin.is_superuser = True
        admin.is_staff = True
        admin.is_active = True
        admin.set_password('1111')
        admin.save()

    def create_pages(self):
        if Page.objects.all().exists() and not FORCE_SYNC:
            return
        for page in settings.PAGES:
            page_obj, _ = Page.objects.get_or_create(url=page[0], title=page[1])
            try:
                page_obj.template = page[2]
                page_obj.save()
            except IndexError:
                pass

    def create_categories(self):
        if Category.objects.all().exists() and not FORCE_SYNC:
            return
        for category in settings.CATEGORIES:
            Category.objects.get_or_create(title=category)

    def create_catalogs(self):
        if Catalog.objects.all().exists() and not FORCE_SYNC:
            return
        for catalog in settings.CATALOGS:
            Catalog.objects.get_or_create(title=catalog)

    def create_top_menu(self):
        if Menu.objects.all().exists() and not FORCE_SYNC:
            return
        menu, _ = Menu.objects.get_or_create(menu_type=Menu.MENU_TOP)
        for menu_item in settings.TOP_MENU:
            menu_item_obj, _ = MenuItem.objects.get_or_create(
                menu=menu, 
                url=menu_item[0], 
                name=menu_item[1]
                )
            menu_item_obj.sorting = menu_item[2]
            menu_item_obj.save()

    def create_bottom_menu(self):
        if Menu.objects.all().exists() and not FORCE_SYNC:
            return
        menu, _ = Menu.objects.get_or_create(menu_type=Menu.MENU_BOTTOM)
        for menu_item in settings.BOTTOM_MENU:
            menu_item_obj, _ = MenuItem.objects.get_or_create(
                menu=menu, 
                url=menu_item[0], 
                name=menu_item[1]
                )
            menu_item_obj.sorting = menu_item[2]
            menu_item_obj.save()

    def create_carousel(self):
        if Carousel.objects.all().exists() and not FORCE_SYNC:
            return
        carousel, _ = Carousel.objects.get_or_create(carousel_type=Carousel.CAROUSEL)
        CarouselItem.objects.filter(carousel=carousel).delete()
        for carousel_item in settings.CAROUSEL:
            carousel_item_obj= CarouselItem.objects.create(
                carousel=carousel, 
                img=carousel_item[0],
                sorting=carousel_item[1]
                )

    def create_site(self):
        if SiteProfile.objects.all().exists() and not FORCE_SYNC:
            return
        site = Site.objects.get(id=settings.SITE_ID)
        site_profile, _ = SiteProfile.objects.get_or_create(site=site)
        site_profile.phone = '+79876543210'
        site_profile.skype = '+79876543210'
        site_profile.email = 'salon-buton@gmail.com'
        site_profile.assist_url = 'https://test.paysecure.ru/pay/order.cfm'
        #site_profile.assist_url = 'https://payments287.paysecure.ru/pay/order.cfm'
        site_profile.assist_merchant_id = '899683'
        site_profile.contact_map = '<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=NH8203s5SXKNgOV9lt8629Q6lyg90Fqx&height=400&lang=ru_RU&sourceType=constructor"></script>'
        
        site_profile.support_email = 'buton.salonfloristicki@yandex.ru'
        site_profile.save()

    def create_discount(self):
        if Discount.objects.all().exists() and not FORCE_SYNC:
            return
        discount, _ = Discount.objects.get_or_create(
            action=Discount.DISCOUNT_ACTION_AUTH,
            )
        discount.amount = 5
        discount.active = True
        discount.save()
        discount, _ = Discount.objects.get_or_create(
            action=Discount.DISCOUNT_ACTION_FAMILY_BIRTHDAY,
            )
        discount.amount = 2
        discount.active = True
        discount.save()

    def handle(self, *args, **kwargs):
        call_command('migrate', interactive=False)
        call_command('collectstatic', interactive=False)

        self.create_users()
        print "CREATE USERS"
        self.create_pages()
        print "CREATE PAGES"
        self.create_categories()
        print "CREATE CATEGORIES"
        self.create_catalogs()
        print "CREATE CATALOGS"
        self.create_top_menu()
        self.create_bottom_menu()
        print "CREATE MENU"
        self.create_carousel()
        self.create_site()
        self.create_discount()

