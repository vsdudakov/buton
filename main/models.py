#-*- encoding: utf-8 -*-
from slugify import slugify
from ckeditor_uploader.fields import RichTextUploadingField

from django.db import models
from django.utils.translation import ugettext_lazy as _ul
from django.contrib.sites.models import Site


class DateBaseModel(models.Model):
    creation_date = models.DateTimeField(verbose_name=_ul(u'Дата создания'), auto_now_add=True)
    modification_date = models.DateTimeField(verbose_name=_ul(u'Дата изменения'), auto_now=True)

    class Meta:
        abstract = True


class ObjBaseModel(DateBaseModel):
    is_published = models.BooleanField(verbose_name=_ul(u'Опубликован?'), default=True)

    class Meta:
        abstract = True


class SeoBaseModel(ObjBaseModel):
    slug = models.SlugField(verbose_name=_ul(u'Слаг'), max_length=255, unique=True, blank=True)
    title = models.CharField(verbose_name=_ul(u'Заголовок'), max_length=255, unique=True)

    seo_title = models.CharField(verbose_name=_ul(u'Заголовок (meta тег)'), max_length=255, null=True, blank=True)
    seo_keywords = models.CharField(verbose_name=_ul(u'Ключевые слова (meta тег)'), max_length=255, null=True, blank=True)
    seo_description = models.CharField(verbose_name=_ul(u'Описание (meta тег)'), max_length=255, null=True, blank=True)
    seo_author = models.CharField(verbose_name=_ul(u'Автор (meta тег)'), max_length=255, null=True, blank=True)

    seo_changefreq = models.CharField(verbose_name=_ul(u'Частота изменений (sitemap.xml)'), max_length=255, default="daily")
    seo_priority = models.FloatField(verbose_name=_ul(u'Приоритет (sitemap.xml)'), default=0.6)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super(SeoBaseModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True


def get_profile(self):
    try:
        return self.profile
    except Exception:
        return SiteProfile.objects.create(site=self)

Site.add_to_class('get_profile', get_profile)


class SiteProfile(DateBaseModel):
    HTTP_METHOD = 'http://'
    HTTPS_METHOD = 'https://'
    HTTP_METHODS = (
        (HTTP_METHOD, _ul(u'http')),
        (HTTPS_METHOD, _ul(u'https')),
    )

    site = models.OneToOneField(Site, related_name='profile')

    http_methog = models.CharField(verbose_name=_ul(u'Http метод'), max_length=255, choices=HTTP_METHODS, default=HTTP_METHOD)

    robots = models.TextField(verbose_name=_ul(u'Содержимое robots.txt'), default="User-agent: *\nDisallow: /")

    yandex_verification = models.TextField(verbose_name=_ul(u'Скрипт yandex верификации'), null=True, blank=True)
    google_verification = models.TextField(verbose_name=_ul(u'Скрипт google верификации'), null=True, blank=True)
    stat_yandex = models.TextField(verbose_name=_ul(u'Скрипт yandex метрики'), null=True, blank=True)
    stat_google = models.TextField(verbose_name=_ul(u'Скрипт google аналитики'), null=True, blank=True)

    contact_map = models.TextField(verbose_name=_ul(u'Скрипт карты для страницы контакты'), null=True, blank=True)

    email = models.EmailField(verbose_name=_ul(u'Email'), null=True, blank=True)
    email2 = models.EmailField(verbose_name=_ul(u'Email 2'), null=True, blank=True)
    phone = models.CharField(verbose_name=_ul(u'Телефон'), max_length=255, null=True, blank=True)
    phone2 = models.CharField(verbose_name=_ul(u'Телефон 2'), max_length=255, null=True, blank=True)
    skype = models.CharField(verbose_name=_ul(u'Skype'), max_length=255, null=True, blank=True)

    assist_url = models.CharField(verbose_name=_ul(u'Assist URL'), max_length=255, null=True, blank=True)
    assist_merchant_id = models.CharField(verbose_name=_ul(u'Assist Merchant Id'), max_length=255, null=True, blank=True)

    vkontakte = models.CharField(verbose_name=_ul(u'Ссылка на Вконтакте'), max_length=255, null=True, blank=True)
    facebook = models.CharField(verbose_name=_ul(u'Ссылка на Facebook'), max_length=255, null=True, blank=True)
    twitter = models.CharField(verbose_name=_ul(u'Ссылка на Твиттер'), max_length=255, null=True, blank=True)
    google = models.CharField(verbose_name=_ul(u'Ссылка на Google +'), max_length=255, null=True, blank=True)
    livejournal = models.CharField(verbose_name=_ul(u'Ссылка на LiveJournal'), max_length=255, null=True, blank=True)
    odnoklassniki = models.CharField(verbose_name=_ul(u'Ссылка на Одноклассники'), max_length=255, null=True, blank=True)
    instagramm  = models.CharField(verbose_name=_ul(u'Ссылка на Instagramm'), max_length=255, null=True, blank=True)

    support_email = models.EmailField(verbose_name=_ul(u'Email для отправки сообщений'), null=True, blank=True)
    manager_email = models.EmailField(verbose_name=_ul(u'Email менеджера'), null=True, blank=True)

    member = RichTextUploadingField(verbose_name=_ul(u'Текст для членов семьи'), null=True, blank=True)

    class Meta:
        verbose_name = _ul(u'Профайл сайта')
        verbose_name_plural = _ul(u'Профайлы сайтов')

    def __unicode__(self):
        return u"%s" % self.site


class Feedback(DateBaseModel):
    QUESTION_TYPE = 0
    REVIEW_TYPE = 1
    FEEDBACK_TYPES = (
        (QUESTION_TYPE, _ul(u'Задать вопрос')),
        (REVIEW_TYPE, _ul(u'Оставить отзыв')),
    )
    feedback_type = models.PositiveIntegerField(verbose_name=_ul(u'Тип обратной связи'), choices=FEEDBACK_TYPES, default=QUESTION_TYPE)
    name = models.CharField(verbose_name=_ul(u'Как к Вам можно обратиться?'), max_length=255)
    photo = models.ImageField(verbose_name=_ul(u'Ваше Фото'), upload_to='uploads/feedbacks/', null=True, blank=True)
    phone = models.CharField(verbose_name=_ul(u'Ваш мобильный телефон или email'), max_length=255)
    question = models.TextField(verbose_name=_ul(u'Ваши впечатления'))

    class Meta:
        verbose_name = _ul(u'Обратная связь и отзыв')
        verbose_name_plural = _ul(u'Обратная связь и отзывы')

    def __unicode__(self):
        return u"%s - %s" % (self.name, self.phone)


class Banner(ObjBaseModel):
    PLACE_INDEX = '0'
    PLACES = (
        (PLACE_INDEX, _ul(u'На главной "первым товаром"')),
    )

    place = models.CharField(verbose_name=_ul(u'Место'), max_length=255, unique=True, choices=PLACES)
    title = models.CharField(verbose_name=_ul(u'Заголовок'), max_length=255, null=True, blank=True)
    img = models.ImageField(verbose_name=_ul(u'Картинка'), upload_to='uploads/banners/', null=True, blank=True, help_text=_ul(u'Размеры картинки (ширина ~575px, высота ~225px)'))
    alt = models.CharField(verbose_name=_ul(u'Alt картинки'), max_length=255, null=True, blank=True)
    link = models.URLField(verbose_name=_ul(u'Ссылка'), null=True, blank=True)

    data = models.TextField(verbose_name=_ul(u'Cкриптовые данные баннера'), null=True, blank=True)

    class Meta:
        verbose_name = _ul(u'Баннер')
        verbose_name_plural = _ul(u'Баннеры')

    def __unicode__(self):
        return self.place


class Page(SeoBaseModel):
    url = models.CharField(verbose_name=_ul(u'Путь'), max_length=255, unique=True)
    template = models.CharField(verbose_name=_ul(u'Шаблон'), max_length=255, default='main/page.html')
    description = RichTextUploadingField(verbose_name=_ul(u'Описание'), null=True, blank=True)
    banners = models.ManyToManyField(Banner, verbose_name=_ul(u'Баннеры'), blank=True)

    def get_banners(self):
        banners = self.banners.filter(is_published=True)
        return {banner.place: banner.data for banner in banners}

    class Meta:
        verbose_name = _ul(u'Страница')
        verbose_name_plural = _ul(u'Страницы')

    def __unicode__(self):
        return self.url


class Menu(DateBaseModel):
    MENU_TOP = 0
    MENU_BOTTOM = 1
    MENU_TYPES = (
        (MENU_TOP, _ul(u'Верхнее меню')),
        (MENU_BOTTOM, _ul(u'Нижнее меню')),
    )
    menu_type = models.PositiveIntegerField(verbose_name=_ul(u'Тип меню'), choices=MENU_TYPES, unique=True)

    def get_items(self):
        return MenuItem.objects.filter(menu=self, is_published=True).order_by('sorting')

    class Meta:
        verbose_name = _ul(u'Меню')
        verbose_name_plural = _ul(u'Меню')

    def __unicode__(self):
        return self.get_menu_type_display()


class MenuItem(ObjBaseModel):
    menu = models.ForeignKey(Menu, verbose_name=_ul(u'Меню'))
    name = models.CharField(verbose_name=_ul(u'Имя'), max_length=255)
    url = models.CharField(verbose_name=_ul(u'Путь'), max_length=255)
    sorting = models.PositiveIntegerField(verbose_name=_ul(u'Сортировка'), default=0)

    class Meta:
        verbose_name = _ul(u'Пункт меню')
        verbose_name_plural = _ul(u'Пункты меню')

    def __unicode__(self):
        return u"%s / %s" % (self.menu, self.name)


class Carousel(DateBaseModel):
    CAROUSEL = 0
    CAROUSEL_TYPES = (
        (CAROUSEL, _ul(u'Главная карусель')),
    )
    carousel_type = models.PositiveIntegerField(verbose_name=_ul(u'Тип карусели'), choices=CAROUSEL_TYPES, unique=True)

    def get_items(self):
        return CarouselItem.objects.filter(carousel=self, is_published=True).order_by('sorting')

    class Meta:
        verbose_name = _ul(u'Карусель')
        verbose_name_plural = _ul(u'Карусель')

    def __unicode__(self):
        return u'%s' % self.get_carousel_type_display()


class CarouselItem(ObjBaseModel):
    carousel = models.ForeignKey(Carousel, verbose_name=_ul(u'Карусель'))
    img = models.ImageField(verbose_name=_ul(u'Картинка'), help_text=_ul(u'Размеры картинки (ширина ~1072px, высота ~680px)'), upload_to='uploads/carousel/', null=True, blank=True)
    sorting = models.PositiveIntegerField(verbose_name=_ul(u'Сортировка'), default=0)
    alt = models.CharField(verbose_name=_ul(u'Alt картинки'), max_length=255, null=True, blank=True)
    link = models.URLField(verbose_name=_ul(u'Ссылка'), null=True, blank=True)

    class Meta:
        verbose_name = _ul(u'Картинка карусели')
        verbose_name_plural = _ul(u'Картинка карусели')

    def __unicode__(self):
        return u"%s / %s" % (self.carousel, self.img)


class Call(DateBaseModel):
    name = models.CharField(verbose_name=_ul(u'Имя'), max_length=255, null=True, blank=True)
    phone = models.CharField(verbose_name=_ul(u'Телефон (+79101010101)'), max_length=255)
    is_handle = models.BooleanField(verbose_name=_ul(u'Обработан?'), default=False)

    class Meta:
        verbose_name = _ul(u'Заказанный звонок')
        verbose_name_plural = _ul(u'Заказанные звонки')

    def __unicode__(self):
        return u"%s / %s" % (self.name, self.phone)