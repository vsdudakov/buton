#-*- encoding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _ul
from django.utils.safestring import mark_safe
from django.forms.util import ErrorDict
from django.utils.encoding import force_unicode

from main.models import Call, Feedback


def form_fields_to_bootstrap(fields):
    for key in fields:
        field = fields[key]
        if field is None:
            continue
        field.widget.attrs.update({'class': field.widget.attrs.get('class', '') + ' common-style input-item'})
        field.widget.attrs.update({'placeholder': field.label if field.label else ''})
        # if field.__class__.__name__ in (
        #     'DateTimeField', 
        #     'DateField', 
        #     ):
        #     field.widget.attrs.update({'class': field.widget.attrs.get('class', '') + ' datepicker'})
        #     field.widget.attrs.update({'style': field.widget.attrs.get('style', '') + ' width: 100%;'})
        if field.__class__.__name__ in (
            'BooleanField',
            ):
            field.widget.attrs.update({'class': field.widget.attrs.get('class', '') + ' checkbox-item'})
        if field.__class__.__name__ in (
                'ChoiceField', 
                'ModelChoiceField',
                'MultipleChoiceField', 
                'ModelMultipleChoiceField'
            ):
            field.widget.attrs.update({'style': field.widget.attrs.get('style', '') + ' width: 100%;'})
        if field.label and field.required:
            field.label += ' *'


class CustomErrorDict(ErrorDict):
    def __init__(self, form, iterable=None, **kwargs):
        self.form = form
        super(CustomErrorDict, self).__init__(iterable, **kwargs)

    def as_ul(self):
        if not self:
            return u''

        def humanify(field_name):
            try:
                return self.form.fields[field_name].label or field_name
            except:
                if field_name == '__all__':
                    return u'Ошибка'
                return field_name

        # main logic is copied from the original ErrorDict:
        return mark_safe(u'<ul class="errorlist">%s</ul>'
                % ''.join([u'<li>%s%s</li>' % (humanify(k), force_unicode(v))
                    for k, v in self.items()]))


class BootstrapForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(BootstrapForm, self).__init__(*args, **kwargs)
        self.pre_init(*args, **kwargs)
        form_fields_to_bootstrap(self.fields)  
        self.post_init(*args, **kwargs)

    def pre_init(self, *args, **kwargs):
        pass  

    def post_init(self, *args, **kwargs):
        pass  

    @property
    def errors(self):
        return CustomErrorDict(self, super(forms.Form, self).errors)


class BootstrapModelForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BootstrapModelForm, self).__init__(*args, **kwargs)
        self.pre_init(*args, **kwargs)
        form_fields_to_bootstrap(self.fields)  
        self.post_init(*args, **kwargs)

    def pre_init(self, *args, **kwargs):
        pass 

    def post_init(self, *args, **kwargs):
        pass  

    @property
    def errors(self):
        return CustomErrorDict(self, super(forms.ModelForm, self).errors)


class CallForm(BootstrapModelForm):

    class Meta:
        model = Call
        fields = ('phone', 'name')


class FeedbackForm(BootstrapModelForm):

    class Meta:
        model = Feedback
        exclude = ()
