#-*- encoding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _ul
from django.contrib.sites.models import Site

from main.models import Banner, Page, SiteProfile, Menu, MenuItem, Carousel, CarouselItem, Call, Feedback


class PageAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'url',
                'title',
                'description',
                'is_published',
                'template',
            )
        }),
        (_ul(u'Seo'), {
            'fields': (
                'seo_title',
                'seo_description', 
                'seo_keywords', 
                'seo_author'
                )
        }),
    )
    exclude = ('slug',)

    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super(PageAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        request = kwargs['request']
        if request.resolver_match.args:
            page_id = request.resolver_match.args[0]
            url = Page.objects.filter(id=page_id).values_list('url', flat=True)
            if url:
                url = url[0]
            if url == '/' and db_field.name == 'description':
                formfield.widget = forms.HiddenInput()
        return formfield


class MenuItemInline(admin.StackedInline):
    model = MenuItem
    classes = ('collapse open',)
    inline_classes = ('collapse open',)


class MenuAdmin(admin.ModelAdmin):
    inlines = (
        MenuItemInline,
    )


class CarouselItemInline(admin.StackedInline):
    model = CarouselItem
    classes = ('collapse open',)
    inline_classes = ('collapse open',)


class CarouselAdmin(admin.ModelAdmin):
    inlines = (
        CarouselItemInline,
    )


class SiteProfileInline(admin.StackedInline):
    model = SiteProfile
    fieldsets = (
        (_ul(u'Seo'), {
            'fields': (
                'http_methog',
                'robots',
                'yandex_verification', 
                'google_verification', 
                'stat_yandex',
                'stat_google',
                )
        }),
        (_ul(u'Контактная информация'), {
            'fields': (
                'email',
                'email2',
                'phone', 
                'phone2', 
                'skype',
                'contact_map'
                )
        }),
        (_ul(u'Ссылки на соц. сети'), {
            'fields': (
                'vkontakte',
                'facebook',
                'twitter', 
                'livejournal', 
                'odnoklassniki',
                )
        }),
        (_ul(u'Настройка email'), {
            'fields': (
                'support_email',
                'manager_email',
                )
        }),
        (_ul(u'Настройка assist'), {
            'fields': (
                'assist_url',
                'assist_merchant_id',
                )
        }),
        (_ul(u'Другое'), {
            'fields': (
                'member',
                )
        }),
    )


class SiteAdmin(admin.ModelAdmin):
    inlines = (
        SiteProfileInline,
    )


class FeedbackAdmin(admin.ModelAdmin):
    list_display = (
        'feedback_type',
        'name',
        'creation_date',
    )
    list_filter = (
        'feedback_type',
    )

admin.site.register(Banner)
admin.site.register(Page, PageAdmin)
admin.site.register(Menu, MenuAdmin)
admin.site.register(Carousel, CarouselAdmin)
admin.site.unregister(Site)
admin.site.register(Site, SiteAdmin)
admin.site.register(Call)
admin.site.register(Feedback, FeedbackAdmin)