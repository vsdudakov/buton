# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0033_auto_20151217_1531'),
    ]

    operations = [
        migrations.AddField(
            model_name='carouselitem',
            name='link',
            field=models.URLField(null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True),
        ),
    ]
