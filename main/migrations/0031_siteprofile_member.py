# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0030_auto_20151209_1611'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteprofile',
            name='member',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name='\u0422\u0435\u043a\u0441\u0442 \u0434\u043b\u044f \u0447\u043b\u0435\u043d\u043e\u0432 \u0441\u0435\u043c\u044c\u0438', blank=True),
        ),
    ]
