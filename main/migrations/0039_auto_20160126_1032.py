# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0038_banner_alt'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='photo',
            field=models.ImageField(upload_to=b'uploads/feedbacks/', null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='phone',
            field=models.CharField(max_length=255, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d \u0438\u043b\u0438 email'),
        ),
    ]
