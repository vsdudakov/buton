# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SiteProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('site', models.OneToOneField(related_name='profile', to='sites.Site')),
            ],
            options={
                'verbose_name': '\u041f\u0440\u043e\u0444\u0430\u0439\u043b \u0441\u0430\u0439\u0442\u0430',
                'verbose_name_plural': '\u041f\u0440\u043e\u0444\u0430\u0439\u043b\u044b \u0441\u0430\u0439\u0442\u043e\u0432',
            },
        ),
    ]
