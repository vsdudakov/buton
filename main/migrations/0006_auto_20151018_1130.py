# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20151018_1130'),
    ]

    operations = [
        migrations.RenameField(
            model_name='siteprofile',
            old_name='google_yandex',
            new_name='stat_google',
        ),
    ]
