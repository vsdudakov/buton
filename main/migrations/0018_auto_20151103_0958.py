# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0017_carouselitem_alt'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='description',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
