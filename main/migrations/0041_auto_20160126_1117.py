# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0040_auto_20160126_1038'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedback',
            name='feedback_type',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0422\u0438\u043f \u043e\u0431\u0440\u0430\u0442\u043d\u043e\u0439 \u0441\u0432\u044f\u0437\u0438', choices=[(0, '\u0417\u0430\u0434\u0430\u0442\u044c \u0432\u043e\u043f\u0440\u043e\u0441'), (1, '\u041e\u0441\u0442\u0430\u0432\u0438\u0442\u044c \u043e\u0442\u0437\u044b\u0432')]),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='name',
            field=models.CharField(max_length=255, verbose_name='\u041a\u0430\u043a \u043a \u0412\u0430\u043c \u043c\u043e\u0436\u043d\u043e \u043e\u0431\u0440\u0430\u0442\u0438\u0442\u044c\u0441\u044f?'),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='phone',
            field=models.CharField(max_length=255, verbose_name='\u0412\u0430\u0448 \u043c\u043e\u0431\u0438\u043b\u044c\u043d\u044b\u0439 \u0442\u0435\u043b\u0435\u0444\u043e\u043d \u0438\u043b\u0438 email'),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='photo',
            field=models.ImageField(upload_to=b'uploads/feedbacks/', null=True, verbose_name='\u0412\u0430\u0448\u0435 \u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='question',
            field=models.TextField(verbose_name='\u0412\u0430\u0448\u0438 \u0432\u043f\u0435\u0447\u0430\u0442\u043b\u0435\u043d\u0438\u044f'),
        ),
    ]
