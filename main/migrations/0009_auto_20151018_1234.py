# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_auto_20151018_1159'),
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('is_published', models.BooleanField(default=True, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d?')),
                ('place', models.CharField(unique=True, max_length=255, verbose_name='\u041c\u0435\u0441\u0442\u043e')),
                ('data', models.TextField(null=True, verbose_name='\u0414\u0430\u043d\u043d\u044b\u0435 \u0431\u0430\u043d\u043d\u0435\u0440\u0430', blank=True)),
            ],
            options={
                'verbose_name': '\u0411\u0430\u043d\u043d\u0435\u0440 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b',
                'verbose_name_plural': '\u0411\u0430\u043d\u043d\u0435\u0440\u044b \u0441\u0442\u0440\u0430\u043d\u0438\u0446',
            },
        ),
        migrations.AddField(
            model_name='page',
            name='banners',
            field=models.ManyToManyField(to='main.Banner', verbose_name='\u0411\u0430\u043d\u043d\u0435\u0440\u044b', blank=True),
        ),
    ]
