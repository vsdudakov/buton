# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0018_auto_20151103_0958'),
    ]

    operations = [
        migrations.CreateModel(
            name='Call',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('phone', phonenumber_field.modelfields.PhoneNumberField(max_length=128, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('is_handle', models.BooleanField(default=False, verbose_name='\u041e\u0431\u0440\u0430\u0431\u043e\u0442\u0430\u043d?')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437\u0430\u043d\u043d\u044b\u0439 \u0437\u0432\u043e\u043d\u043e\u043a',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u0430\u043d\u043d\u044b\u0435 \u0437\u0432\u043e\u043d\u043a\u0438',
            },
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='google',
            field=models.CharField(max_length=255, null=True, verbose_name='Google +', blank=True),
        ),
    ]
