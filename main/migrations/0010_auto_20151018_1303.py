# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20151018_1234'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='banner',
            options={'verbose_name': '\u0411\u0430\u043d\u043d\u0435\u0440 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', 'verbose_name_plural': '\u0411\u0430\u043d\u043d\u0435\u0440\u044b \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b'},
        ),
        migrations.AlterModelOptions(
            name='page',
            options={'verbose_name': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430 \u0441\u0430\u0439\u0442\u0430', 'verbose_name_plural': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0441\u0430\u0439\u0442\u0430'},
        ),
        migrations.AddField(
            model_name='banner',
            name='img',
            field=models.ImageField(upload_to=b'uploads/banners/', null=True, verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='banner',
            name='link',
            field=models.URLField(null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='banner',
            name='title',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True),
        ),
        migrations.AlterField(
            model_name='banner',
            name='data',
            field=models.TextField(null=True, verbose_name='C\u043a\u0440\u0438\u043f\u0442\u043e\u0432\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435 \u0431\u0430\u043d\u043d\u0435\u0440\u0430', blank=True),
        ),
    ]
