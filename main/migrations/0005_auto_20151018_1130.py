# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_siteprofile_robots'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteprofile',
            name='google_verification',
            field=models.TextField(null=True, verbose_name='\u0421\u043a\u0440\u0438\u043f\u0442 google \u0432\u0435\u0440\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u0438', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='google_yandex',
            field=models.TextField(null=True, verbose_name='\u0421\u043a\u0440\u0438\u043f\u0442 google \u0430\u043d\u0430\u043b\u0438\u0442\u0438\u043a\u0438', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='http_methog',
            field=models.CharField(default=b'http://', max_length=255, verbose_name='Http \u043c\u0435\u0442\u043e\u0434', choices=[(b'http://', 'http'), (b'https://', 'https')]),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='stat_yandex',
            field=models.TextField(null=True, verbose_name='\u0421\u043a\u0440\u0438\u043f\u0442 yandex \u043c\u0435\u0442\u0440\u0438\u043a\u0438', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='yandex_verification',
            field=models.TextField(null=True, verbose_name='\u0421\u043a\u0440\u0438\u043f\u0442 yandex \u0432\u0435\u0440\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u0438', blank=True),
        ),
    ]
