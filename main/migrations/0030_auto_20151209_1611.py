# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0029_auto_20151209_0020'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='img',
            field=models.ImageField(help_text='\u0420\u0430\u0437\u043c\u0435\u0440\u044b \u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0438 (\u0448\u0438\u0440\u0438\u043d\u0430 ~575px, \u0432\u044b\u0441\u043e\u0442\u0430 ~225px)', upload_to=b'uploads/banners/', null=True, verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430', blank=True),
        ),
    ]
