# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0025_auto_20151204_1021'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteprofile',
            name='assist_merchant_id',
            field=models.CharField(max_length=255, null=True, verbose_name='Assist Merchant Id', blank=True),
        ),
    ]
