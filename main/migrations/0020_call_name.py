# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0019_auto_20151203_0008'),
    ]

    operations = [
        migrations.AddField(
            model_name='call',
            name='name',
            field=phonenumber_field.modelfields.PhoneNumberField(default='', max_length=128, verbose_name='\u0418\u043c\u044f'),
            preserve_default=False,
        ),
    ]
