# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0026_siteprofile_assist_merchant_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteprofile',
            name='email_host',
            field=models.CharField(max_length=255, null=True, verbose_name='Email host', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='email_host_password',
            field=models.CharField(max_length=255, null=True, verbose_name='Email host password', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='email_host_user',
            field=models.CharField(max_length=255, null=True, verbose_name='Email host user', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='email_port',
            field=models.IntegerField(null=True, verbose_name='Email port', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='email_use_ssl',
            field=models.BooleanField(default=False, verbose_name='Email use ssl'),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='email_use_tls',
            field=models.BooleanField(default=False, verbose_name='Email use tls'),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='support_email',
            field=models.EmailField(max_length=254, null=True, verbose_name='Email \u0434\u043b\u044f \u043e\u0442\u043f\u0440\u0430\u0432\u043a\u0438 \u0438 \u043f\u043e\u043b\u0443\u0447\u0435\u043d\u0438\u044f \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0439', blank=True),
        ),
    ]
