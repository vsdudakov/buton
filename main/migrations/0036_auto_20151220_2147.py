# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0035_auto_20151220_2106'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='siteprofile',
            name='email_host',
        ),
        migrations.RemoveField(
            model_name='siteprofile',
            name='email_host_password',
        ),
        migrations.RemoveField(
            model_name='siteprofile',
            name='email_host_user',
        ),
        migrations.RemoveField(
            model_name='siteprofile',
            name='email_port',
        ),
        migrations.RemoveField(
            model_name='siteprofile',
            name='email_use_ssl',
        ),
        migrations.RemoveField(
            model_name='siteprofile',
            name='email_use_tls',
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='manager_email',
            field=models.EmailField(max_length=254, null=True, verbose_name='Email \u043c\u0435\u043d\u0435\u0434\u0436\u0435\u0440\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='siteprofile',
            name='support_email',
            field=models.EmailField(max_length=254, null=True, verbose_name='Email \u0434\u043b\u044f \u043e\u0442\u043f\u0440\u0430\u0432\u043a\u0438 \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0439', blank=True),
        ),
    ]
