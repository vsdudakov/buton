# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20151018_1103'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteprofile',
            name='robots',
            field=models.TextField(default=b'User-agent: *\nDisallow: /', verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435 robots.txt'),
        ),
    ]
