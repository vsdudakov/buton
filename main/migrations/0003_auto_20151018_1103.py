# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_page'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='seo_changefreq',
            field=models.CharField(default=b'daily', max_length=255, verbose_name='\u0427\u0430\u0441\u0442\u043e\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u0439 (sitemap.xml)'),
        ),
        migrations.AddField(
            model_name='page',
            name='seo_priority',
            field=models.FloatField(default=0.6, verbose_name='\u041f\u0440\u0438\u043e\u0440\u0438\u0442\u0435\u0442 (sitemap.xml)'),
        ),
    ]
