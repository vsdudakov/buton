# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0020_call_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='call',
            name='name',
            field=models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f'),
        ),
    ]
