# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0039_auto_20160126_1032'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='feedback',
            options={'verbose_name': '\u041e\u0431\u0440\u0430\u0442\u043d\u0430\u044f \u0441\u0432\u044f\u0437\u044c \u0438 \u043e\u0442\u0437\u044b\u0432', 'verbose_name_plural': '\u041e\u0431\u0440\u0430\u0442\u043d\u0430\u044f \u0441\u0432\u044f\u0437\u044c \u0438 \u043e\u0442\u0437\u044b\u0432\u044b'},
        ),
        migrations.AddField(
            model_name='feedback',
            name='feedback_type',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0422\u0438\u043f \u043e\u0431\u0440\u0430\u0442\u043d\u043e\u0439 \u0441\u0432\u044f\u0437\u0438', choices=[(0, '\u0412\u043e\u043f\u0440\u043e\u0441'), (1, '\u041e\u0442\u0437\u044b\u0432')]),
        ),
    ]
