# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0036_auto_20151220_2147'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteprofile',
            name='instagramm',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 Instagramm', blank=True),
        ),
    ]
