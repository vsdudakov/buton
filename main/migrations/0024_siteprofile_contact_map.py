# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0023_auto_20151203_0951'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteprofile',
            name='contact_map',
            field=models.TextField(null=True, verbose_name='\u0421\u043a\u0440\u0438\u043f\u0442 \u043a\u0430\u0440\u0442\u044b \u0434\u043b\u044f \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u043a\u043e\u043d\u0442\u0430\u043a\u0442\u044b', blank=True),
        ),
    ]
