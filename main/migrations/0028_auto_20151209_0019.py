# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0027_auto_20151206_2255'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carouselitem',
            name='img',
            field=models.ImageField(upload_to=b'uploads/carousel/', null=True, verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430 (\u0448\u0438\u0440\u0438\u043d\u0430 ~1072px, \u0432\u044b\u0441\u043e\u0442\u0430 ~680px)', blank=True),
        ),
    ]
