# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0037_siteprofile_instagramm'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='alt',
            field=models.CharField(max_length=255, null=True, verbose_name='Alt \u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0438', blank=True),
        ),
    ]
