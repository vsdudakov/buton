# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0011_auto_20151018_1319'),
    ]

    operations = [
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('is_published', models.BooleanField(default=True, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d?')),
                ('menu_type', models.PositiveIntegerField(verbose_name='\u0422\u0438\u043f \u043c\u0435\u043d\u044e', choices=[(0, '\u0412\u0435\u0440\u0445\u043d\u0435\u0435 \u043c\u0435\u043d\u044e'), (1, '\u041d\u0438\u0436\u043d\u0435\u0435 \u043c\u0435\u043d\u044e')])),
            ],
            options={
                'verbose_name': '\u041c\u0435\u043d\u044e',
                'verbose_name_plural': '\u041c\u0435\u043d\u044e',
            },
        ),
        migrations.CreateModel(
            name='MenuItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('is_published', models.BooleanField(default=True, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d?')),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('url', models.CharField(max_length=255, verbose_name='\u041f\u0443\u0442\u044c')),
                ('sorting', models.PositiveIntegerField(default=0, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430')),
                ('menu', models.ForeignKey(verbose_name='\u041c\u0435\u043d\u044e', to='main.Menu')),
            ],
            options={
                'verbose_name': '\u041f\u0443\u043d\u043a\u0442 \u043c\u0435\u043d\u044e',
                'verbose_name_plural': '\u041f\u0443\u043d\u043a\u0442\u044b \u043c\u0435\u043d\u044e',
            },
        ),
    ]
