# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0034_carouselitem_link'),
    ]

    operations = [
        migrations.AlterField(
            model_name='call',
            name='phone',
            field=models.CharField(max_length=255, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d (+79101010101)'),
        ),
    ]
