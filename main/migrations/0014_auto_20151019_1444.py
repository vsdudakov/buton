# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_remove_menu_is_published'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menu',
            name='menu_type',
            field=models.PositiveIntegerField(unique=True, verbose_name='\u0422\u0438\u043f \u043c\u0435\u043d\u044e', choices=[(0, '\u0412\u0435\u0440\u0445\u043d\u0435\u0435 \u043c\u0435\u043d\u044e'), (1, '\u041d\u0438\u0436\u043d\u0435\u0435 \u043c\u0435\u043d\u044e')]),
        ),
    ]
