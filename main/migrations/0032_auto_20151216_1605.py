# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0031_siteprofile_member'),
    ]

    operations = [
        migrations.AlterField(
            model_name='siteprofile',
            name='phone',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
        ),
        migrations.AlterField(
            model_name='siteprofile',
            name='phone2',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d 2', blank=True),
        ),
    ]
