# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0015_auto_20151019_1541'),
    ]

    operations = [
        migrations.CreateModel(
            name='Carousel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('carousel_type', models.PositiveIntegerField(unique=True, verbose_name='\u0422\u0438\u043f \u043a\u0430\u0440\u0443\u0441\u0435\u043b\u0438', choices=[(0, '\u0413\u043b\u0430\u0432\u043d\u0430\u044f \u043a\u0430\u0440\u0443\u0441\u0435\u043b\u044c')])),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0440\u0443\u0441\u0435\u043b\u044c',
                'verbose_name_plural': '\u041a\u0430\u0440\u0443\u0441\u0435\u043b\u044c',
            },
        ),
        migrations.CreateModel(
            name='CarouselItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('modification_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('is_published', models.BooleanField(default=True, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d?')),
                ('img', models.ImageField(upload_to=b'uploads/carousel/', null=True, verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430', blank=True)),
                ('sorting', models.PositiveIntegerField(default=0, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430')),
                ('carousel', models.ForeignKey(verbose_name='\u041a\u0430\u0440\u0443\u0441\u0435\u043b\u044c', to='main.Carousel')),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430 \u043a\u0430\u0440\u0443\u0441\u0435\u043b\u0438',
                'verbose_name_plural': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430 \u043a\u0430\u0440\u0443\u0441\u0435\u043b\u0438',
            },
        ),
    ]
