# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0022_auto_20151203_0945'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteprofile',
            name='assist_url',
            field=models.CharField(max_length=255, null=True, verbose_name='Assist URL', blank=True),
        ),
        migrations.AlterField(
            model_name='siteprofile',
            name='facebook',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 Facebook', blank=True),
        ),
        migrations.AlterField(
            model_name='siteprofile',
            name='google',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 Google +', blank=True),
        ),
        migrations.AlterField(
            model_name='siteprofile',
            name='livejournal',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 LiveJournal', blank=True),
        ),
        migrations.AlterField(
            model_name='siteprofile',
            name='odnoklassniki',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u041e\u0434\u043d\u043e\u043a\u043b\u0430\u0441\u0441\u043d\u0438\u043a\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='siteprofile',
            name='twitter',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0422\u0432\u0438\u0442\u0442\u0435\u0440', blank=True),
        ),
        migrations.AlterField(
            model_name='siteprofile',
            name='vkontakte',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0412\u043a\u043e\u043d\u0442\u0430\u043a\u0442\u0435', blank=True),
        ),
    ]
