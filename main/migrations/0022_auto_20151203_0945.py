# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0021_auto_20151203_0118'),
    ]

    operations = [
        migrations.AlterField(
            model_name='call',
            name='phone',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d (+79101010101)'),
        ),
    ]
