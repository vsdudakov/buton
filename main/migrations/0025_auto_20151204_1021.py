# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0024_siteprofile_contact_map'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='place',
            field=models.CharField(unique=True, max_length=255, verbose_name='\u041c\u0435\u0441\u0442\u043e', choices=[(b'0', '\u041d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 "\u043f\u0435\u0440\u0432\u044b\u043c \u0442\u043e\u0432\u0430\u0440\u043e\u043c"')]),
        ),
    ]
