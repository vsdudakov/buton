# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0014_auto_20151019_1444'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteprofile',
            name='email',
            field=models.EmailField(max_length=254, null=True, verbose_name='Email', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='email2',
            field=models.EmailField(max_length=254, null=True, verbose_name='Email 2', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='facebook',
            field=models.CharField(max_length=255, null=True, verbose_name='Facebook', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='livejournal',
            field=models.CharField(max_length=255, null=True, verbose_name='LiveJournal', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='odnoklassniki',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041e\u0434\u043d\u043e\u043a\u043b\u0430\u0441\u0441\u043d\u0438\u043a\u0438', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='phone',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='phone2',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d 2', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='skype',
            field=models.CharField(max_length=255, null=True, verbose_name='Skype', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='twitter',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0422\u0432\u0438\u0442\u0442\u0435\u0440', blank=True),
        ),
        migrations.AddField(
            model_name='siteprofile',
            name='vkontakte',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0412\u043a\u043e\u043d\u0442\u0430\u043a\u0442\u0435', blank=True),
        ),
    ]
