# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0032_auto_20151216_1605'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='feedback',
            name='address',
        ),
        migrations.AlterField(
            model_name='call',
            name='name',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0418\u043c\u044f', blank=True),
        ),
    ]
