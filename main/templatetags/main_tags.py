from decimal import Decimal

from django import template
from django.core.urlresolvers import resolve


register = template.Library()


@register.filter
def range(value):
    return xrange(1, value + 1)


@register.filter
def left_show(value):
    if value % 2:
        value += 1
    return (value / 2) % 2


@register.filter
def with_discount(value, discount):
    return value - value * (discount/Decimal(100.0))
