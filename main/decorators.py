#-*- encoding: utf-8 -*-
from django.shortcuts import  redirect
from django.core.urlresolvers import reverse_lazy


def login_required(login_url=None):
    def decorator(f):
        def wrap(request, *args, **kwargs):
            if request.user.is_authenticated():
                return f(request, *args, **kwargs)
            else:
                request.session['show_login_dialog'] = True
                return redirect(reverse_lazy('index'))
        return wrap
    return decorator
