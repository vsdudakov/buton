#-*- encoding: utf-8 -*-
"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = '{{ project }}.{{ file }}.CustomIndexDashboard'
"""
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _ul
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class IndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """
    
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        request = context.get('request', None)

        self.children.append(modules.AppList(
            _ul(u'Сайт'),
            collapsible=True,
            column=1,
            css_classes=('collapse closed',),
            models=('main.models.*', 'django.contrib.sites.models.*'),
        ))

        self.children.append(modules.AppList(
            _ul(u'Статьи'),
            collapsible=True,
            column=1,
            css_classes=('collapse closed',),
            models=('articles.models.*',),
        ))

        self.children.append(modules.AppList(
            _ul(u'Магазин'),
            collapsible=True,
            column=1,
            css_classes=('collapse closed',),
            models=('shop.models.*',),
        ))

        self.children.append(modules.AppList(
            _ul(u'Пользователи'),
            collapsible=True,
            column=1,
            css_classes=('collapse closed',),
            models=('users.models.*',),
        ))

        # self.children.append(modules.LinkList(
        #     _ul(u'Файлы на сервере'),
        #     column=2,
        #     children=[
        #         {
        #             'title': _ul(u'Файлы на сервере'),
        #             'url': '/admin/filebrowser/browse/',
        #             'external': False,
        #         },
        #     ]
        # ))

        self.children.append(modules.RecentActions(
            title=_ul(u'Последние действия'),
            column=2,
            collapsible=False,
            limit=10,
        ))




 



