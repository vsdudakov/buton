#-*- encoding: utf-8 -*-
import os
from django.contrib.sites.shortcuts import get_current_site

from main.models import Menu, Carousel, Banner
from shop.models import Item, OrderItem
from main.forms import CallForm
from users.forms import LoginForm, RegistrationForm
from main.mixins import get_and_reset_session


def main_context_processors(request):
    site = get_current_site(request)
    site_profile = site.get_profile()
    kwargs = {}
    kwargs['meta_canonical'] = site_profile.http_methog + site.domain + request.path
    kwargs['meta_yandex_verification'] = site_profile.yandex_verification
    kwargs['meta_google_verification'] = site_profile.google_verification
    kwargs['meta_site_name'] = site.name
    kwargs['meta_stat_yandex'] = site_profile.stat_yandex
    kwargs['meta_stat_google'] = site_profile.stat_google

    kwargs['contact_map'] = site_profile.contact_map
    kwargs['host'] = site_profile.http_methog + site.domain

    kwargs['site'] = site
    kwargs['site_profile'] = site_profile
    kwargs['path'] = request.path

    kwargs['cart_count'] = len(OrderItem.objects.get_cart(request))

    kwargs['top_menu'] = Menu.objects.filter(menu_type=Menu.MENU_TOP).first()
    kwargs['bottom_menu'] = Menu.objects.filter(menu_type=Menu.MENU_BOTTOM).first()
    kwargs['carousel'] = Carousel.objects.filter(carousel_type=Carousel.CAROUSEL).first()

    if not request.user.is_authenticated():
        kwargs['login_form'] = LoginForm(data=get_and_reset_session(request, 'LoginForm'))
        kwargs['registration_form'] = RegistrationForm(data=get_and_reset_session(request, 'RegistrationForm'))

    if request.user.is_authenticated():
        kwargs['call_form'] = CallForm(initial=dict(
                name=request.user.get_full_name(),
                phone=request.user.phone
            ), data=get_and_reset_session(request, 'CallForm'))
    else:
        kwargs['call_form'] = CallForm(data=get_and_reset_session(request, 'CallForm'))

    kwargs['show_login_dialog'] = get_and_reset_session(request, 'show_login_dialog')
    kwargs['index_banner'] = Banner.objects.filter(place=Banner.PLACE_INDEX).first()

    kwargs['social_vkontakte'] = site_profile.vkontakte
    kwargs['social_facebook'] = site_profile.facebook
    kwargs['social_twitter'] = site_profile.twitter
    kwargs['social_google'] = site_profile.google
    kwargs['social_livejournal'] = site_profile.livejournal
    kwargs['social_odnoklassniki'] = site_profile.odnoklassniki
    kwargs['social_instangramm'] = site_profile.instagramm
    return kwargs
