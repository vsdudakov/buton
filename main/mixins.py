#-*- encoding: utf-8 -*-
from django.shortcuts import redirect
from django.views.generic.edit import FormMixin
from django.views.generic.detail import SingleObjectMixin
from django.http import Http404


class SeoMixin(object):

    def get_context_seo_data(self, **kwargs): 
        obj = kwargs.get('object', None)
        if obj is not None:
            kwargs['meta_title'] = getattr(obj, 'seo_title')
            kwargs['meta_description'] = getattr(obj, 'seo_description')
            kwargs['meta_keywords'] = getattr(obj, 'seo_keywords')
            kwargs['meta_author'] = getattr(obj, 'seo_author')
            kwargs['meta_image'] = ''
        kwargs['meta_prev'] = ''
        kwargs['meta_next'] = ''  
        return kwargs


def get_and_reset_session(request, name):
    data = request.session.get(name, None)
    request.session[name] = None
    return data


class DialogFormMixin(FormMixin):
    unsuccess_url = None
    template_name = ''

    def get_unsuccess_url(self):
        return self.unsuccess_url or self.success_url

    def get_session_key(self):
        return self.form_class.__name__

    def form_invalid(self, form):
        self.request.session[self.get_session_key()] = form.data
        return redirect(self.get_unsuccess_url())

    def get(self, *args, **kwargs):
        raise Http404
