#-*- encoding: utf-8 -*-
from django.contrib.sitemaps.views import sitemap
from django.contrib.sitemaps import Sitemap as BaseSitemap
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.sites.shortcuts import get_current_site

from main.models import Page
from shop.models import Catalog, Item
from articles.models import Category, Article


class SiteMap(BaseSitemap):
    url_type = None

    def get_urls(self, page=1, site=None, protocol=None):
        urls = super(SiteMap, self).get_urls(page=page, site=site, protocol=protocol)
        for url in urls:
            url['type'] = self.url_type
        return urls


class PageSitemap(SiteMap):
    changefreq = "daily"
    priority = 0.6
    url_type = 'page'

    def items(self):
        return Page.objects.filter(is_published=True)

    def lastmod(self, obj):
        return obj.modification_date

    def location(self, obj):
        return obj.url


class CatalogSitemap(SiteMap):
    changefreq = "daily"
    priority = 0.6
    url_type = 'catalog'

    def items(self):
        return Catalog.objects.filter(is_published=True)

    def lastmod(self, obj):
        return obj.modification_date

    def location(self, obj):
        return reverse('items', kwargs={'slug': obj.slug})


class ItemSitemap(SiteMap):
    changefreq = "daily"
    priority = 0.6
    url_type = 'item'

    def items(self):
        return Item.objects.filter(is_published=True)

    def lastmod(self, obj):
        return obj.modification_date

    def location(self, obj):
        return reverse('items-detail', kwargs={'slug': obj.slug})


class CategorySitemap(SiteMap):
    changefreq = "daily"
    priority = 0.6
    url_type = 'category'

    def items(self):
        return Category.objects.filter(is_published=True)

    def lastmod(self, obj):
        return obj.modification_date

    def location(self, obj):
        return reverse('articles', kwargs={'slug': obj.slug})


class ArticleSitemap(SiteMap):
    changefreq = "daily"
    priority = 0.6
    url_type = 'article'

    def items(self):
        return Article.objects.filter(is_published=True)

    def lastmod(self, obj):
        return obj.modification_date

    def location(self, obj):
        return reverse('articles-detail', kwargs={'slug': obj.slug})


def sitemap_xml(request, *args, **kwargs):
    sitemaps = {
        'pages': PageSitemap,
        'catalogs': CatalogSitemap,
        'items': ItemSitemap,
        'categories': CategorySitemap,
        'articles': ArticleSitemap,
    }
    kwargs['sitemaps'] = sitemaps
    return sitemap(request, *args, **kwargs)


def robots_txt(request, *args, **kwargs):
    site = get_current_site(request)
    site_profile = site.get_profile()
    return HttpResponse(site_profile.robots, content_type="text/plain")