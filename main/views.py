#-*- encoding: utf-8 -*-
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView, DetailView, CreateView, ListView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site

from main.models import Page, Feedback
from main.mixins import SeoMixin, DialogFormMixin
from main.forms import CallForm, FeedbackForm
from shop.views import ItemList
from users.models import User


class PageView(SeoMixin, DetailView):
    model = Page
    
    def get_template_names(self):
        return [self.object.template]

    def get_object(self, queryset=None):
        url = self.request.path
        return get_object_or_404(self.model, url=url, is_published=True)

    def get_context_data(self, **kwargs):
        kwargs = super(PageView, self).get_context_data(**kwargs)
        kwargs = self.get_context_seo_data(**kwargs) 
        return kwargs


class ContactsView(PageView):

    def get_context_data(self, **kwargs):
        kwargs = super(ContactsView, self).get_context_data(**kwargs)
        initial = {}
        if self.request.user.is_authenticated():
            initial['feedback_type'] = Feedback.QUESTION_TYPE
            initial['name'] = self.request.user.get_full_name()
            initial['phone'] = self.request.user.phone
            initial['address'] = self.request.user.delivery
        kwargs['feedback_form'] = FeedbackForm(initial=initial) 
        return kwargs


class CallView(DialogFormMixin, CreateView):
    form_class = CallForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        result = super(CallView, self).form_valid(form)
        messages.success(self.request, u'Ваша заявка принята. Мы перезвоним вам в ближайшее время.')
        User.objects.send_call(get_current_site(self.request), self.object)
        return result


class ReviewsView(ListView):
    template_name = 'main/reviews.html'
    paginate_by = 10
    model = Feedback

    def get_context_data(self, **kwargs):
        kwargs = super(ReviewsView, self).get_context_data(**kwargs)
        initial = {}
        if self.request.user.is_authenticated():
            initial['feedback_type'] = Feedback.REVIEW_TYPE
            initial['name'] = self.request.user.get_full_name()
            initial['phone'] = self.request.user.phone
            initial['address'] = self.request.user.delivery
        kwargs['feedback_form'] = FeedbackForm(initial=initial) 
        return kwargs

    def get_queryset(self):
        qs = super(ReviewsView, self).get_queryset()
        qs = qs.filter(feedback_type=Feedback.REVIEW_TYPE).order_by('-creation_date')
        return qs


class FeedbackView(DialogFormMixin, CreateView):
    form_class = FeedbackForm
    success_url = reverse_lazy('contacts')

    def get_success_url(self):
        success_url = self.request.GET.get('next')
        if success_url:
            return success_url
        return self.success_url

    def get_unsuccess_url(self):
        unsuccess_url = self.request.GET.get('next')
        if unsuccess_url:
            return unsuccess_url
        return self.unsuccess_url or self.success_url

    def form_valid(self, form):
        feedback_type = form.cleaned_data.get('feedback_type', Feedback.QUESTION_TYPE)
        result = super(FeedbackView, self).form_valid(form)
        if feedback_type == Feedback.QUESTION_TYPE:
            messages.success(self.request, u'Ваше сообщение отправлено. Мы свяжемся с вами в ближайшее время.')
            User.objects.send_feedback(get_current_site(self.request), self.object)
        if feedback_type == Feedback.REVIEW_TYPE:
            messages.success(self.request, u'Спасибо за Ваш отзыв.')
        return result
