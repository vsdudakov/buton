"""
Django settings for buton project.

Generated by 'django-admin startproject' using Django 1.8.5.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'nyhv5e)rj#_ccq2s8t2k(75wyg*vc@ai6@xl^0asu&f895j*+^'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.webdesign',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.sites',
    'django.contrib.sitemaps',

    'grappelli.dashboard',
    'grappelli',
    'filebrowser',
    'django.contrib.admin',

    'ckeditor',
    'ckeditor_uploader',
    'sorl.thumbnail',
    'social_auth',

    'main',
    'users',
    'shop',
    'articles',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',

    'main.middleware.MainMiddleware'
)

ROOT_URLCONF = 'main.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'main', 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                'main.context_processors.main_context_processors',
            ],
        },
    },
]

WSGI_APPLICATION = 'main.wsgi.application'



# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'ru-RU'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATIC_DIRS = (
    os.path.join(BASE_DIR, 'main', 'static'),
)

SITE_ID = 1

AUTH_USER_MODEL = 'users.User'

EMAIL_USE_TLS = False
EMAIL_USE_SSL = True
EMAIL_HOST = 'smtp.mail.ru'
EMAIL_HOST_USER = 'salonbuton@mail.ru'
EMAIL_HOST_PASSWORD = '924042sa'
EMAIL_PORT = 465

# Use Common middleware and md5 hash of pages
# (Instead it, for quickly working read https://docs.djangoproject.com/en/1.7/topics/conditional-view-processing/)
USE_ETAGS = True

GRAPPELLI_ADMIN_TITLE = u'Administration'
GRAPPELLI_INDEX_DASHBOARD = 'main.dashboard.IndexDashboard'

CKEDITOR_JQUERY_URL = '/static/jquery/jquery.min.js'
CKEDITOR_UPLOAD_PATH = "uploads/data/"

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Image'],
            ['Link', 'Unlink'],
            ['RemoveFormat', 'Source']
        ],
        'allowedContent': True,
        'removeFormatAttributes': ''
    }
}

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
    'social_auth.backends.google.GoogleOAuthBackend',
    'social_auth.backends.google.GoogleOAuth2Backend',
    'social_auth.backends.google.GoogleBackend',
    #'social_auth.backends.yahoo.YahooBackend',
    #'social_auth.backends.browserid.BrowserIDBackend',
    #'social_auth.backends.contrib.linkedin.LinkedinBackend',
    #'social_auth.backends.contrib.disqus.DisqusBackend',
    #'social_auth.backends.contrib.livejournal.LiveJournalBackend',
    #'social_auth.backends.contrib.orkut.OrkutBackend',
    #'social_auth.backends.contrib.foursquare.FoursquareBackend',
    #'social_auth.backends.contrib.github.GithubBackend',
    'social_auth.backends.contrib.vk.VKOAuth2Backend',
    'social_auth.backends.contrib.odnoklassniki.OdnoklassnikiBackend',
    #'social_auth.backends.contrib.live.LiveBackend',
    #'social_auth.backends.contrib.skyrock.SkyrockBackend',
    #'social_auth.backends.contrib.yahoo.YahooOAuthBackend',
    #'social_auth.backends.contrib.readability.ReadabilityBackend',
    #'social_auth.backends.contrib.fedora.FedoraBackend',
    #'social_auth.backends.OpenIDBackend',
    'django.contrib.auth.backends.ModelBackend',
)

TWITTER_CONSUMER_KEY         = 'K5AJkuSGPoe3S3srjOMOIlIo2'
TWITTER_CONSUMER_SECRET      = 'vbgOnayTKEol5GccN0rzfch08oB7FhbwOCj8Qbx0g7BkfynP0V'
FACEBOOK_APP_ID              = '1662443370711032'
FACEBOOK_API_SECRET          = 'b91f7eb5c26e9c316457d29f0190b127'
#LINKEDIN_CONSUMER_KEY        = ''
#LINKEDIN_CONSUMER_SECRET     = ''
#ORKUT_CONSUMER_KEY           = ''
#ORKUT_CONSUMER_SECRET        = ''
#GOOGLE_CONSUMER_KEY          = ''
#GOOGLE_CONSUMER_SECRET       = ''
GOOGLE_OAUTH2_CLIENT_ID      = '1029410111867-0lm334ckpk5cufr50tg0ph095d631gbp.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET  = '4AyjC-V97y8wDIFqgK01tzUr'
#FOURSQUARE_CONSUMER_KEY      = ''
#FOURSQUARE_CONSUMER_SECRET   = ''
VK_APP_ID                    = '5174713'
VK_API_SECRET                = 'NODBrcvKh8QwRtbHEria'
#LIVE_CLIENT_ID               = ''
#LIVE_CLIENT_SECRET           = ''
#SKYROCK_CONSUMER_KEY         = ''
#SKYROCK_CONSUMER_SECRET      = ''
#YAHOO_CONSUMER_KEY           = ''
#YAHOO_CONSUMER_SECRET        = ''
#READABILITY_CONSUMER_SECRET  = ''
#READABILITY_CONSUMER_SECRET  = ''
ODNOKLASSNIKI_OAUTH2_CLIENT_KEY = '1243502080'
ODNOKLASSNIKI_OAUTH2_APP_KEY = 'CBAHFPBLEBABABABA'
ODNOKLASSNIKI_OAUTH2_CLIENT_SECRET = '5F5C7ECDE68A3982AB296CD0'

SOCIAL_AUTH_USER_MODEL = 'users.User'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/profile/'

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'



CATEGORIES = ()
CATALOGS = ()
PAGES = ()
TOP_MENU = ()
BOTTOM_MENU = ()
CAROUSEL = ()